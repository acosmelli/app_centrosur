function qrScanner_PT(){
   if (window.navigator.simulator === true) {
     navAlert('Aviso','Simulador NO Soporta BarcodeScanner');
   }else{ 
   //    cordova.plugins.diagnostic.isLocationAvailable(function(available){
   //      if(available){
   //         navigator.geolocation.getCurrentPosition(function(position){
                
                cordova.plugins.barcodeScanner.scan(function(result) {
                  if (!result.cancelled) {
                    var userid = window.atob(localStorage.getItem('uKey'));
                    var qrcode = result.text;
                    var latitude = 0;//position.coords.latitude;
                    var longitude = 0;//position.coords.longitude;
                        
                    $.ajax({
                      type: "POST",
                      url: www+"burnPT",
                      headers: { 'authorization': "token " + token },
                      dataType: "json",
                      data: "profid=" + userid + "&qrcode=" + qrcode + "&longitude=" + longitude+ "&latitude=" + latitude,
                      cache: false,
                      async: true,
                      beforeSend: function (xhr){
                        app.mobileApp.pane.loader.show();
                      },
                      success: function (Object){
                        navigator.vibrate(200);
                        if(Object['status'] == '200'){
                          navAlert('Aviso',Object['data']);
                        }else{
                            if(Object['error'] != 'ERROR-NOT-DATA'){
                                navAlert('Aviso',Object['data']);
                            }else{
                                navAlert('Error','Ha ocurrido un error, por favor volver a intentar.');
                            }
                        }
                      },
                      error: function (xhr, err){
                        navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                      }
                    });
                    app.mobileApp.pane.loader.hide();
                  }
                }, 
                function(error) {
                    //error al tratar de leer qr
                    //navAlert('Error [3]: ' + error.code, error.message);
                });
                
   //         },function(error){
   //             //error al tratar de tomar la geolocalizacion actual
   //             navAlert('Error [2]: ' + error.code, error.message);
   //         },{maximumAge: 30000, timeout: 3000, enableHighAccuracy: true});    
   //      }else{
   //          navAlert('Aviso',"Para poder utilizar el código QR debes tener activado el GPS.");
   //      }
   //   }, function(error){
   //       //error al ver si el gps estaba activado
   //       navAlert('Error [1]: ' + error.code, error.message);
   //   });
   }
}

function checkStatusNot(){
     var userid = window.atob(localStorage.getItem('uKey'));
     var token = localStorage.getItem('token'); 
    
     $.ajax({
        type: "GET",
        url: www+"checkStatusNot?userid="+userid,
        headers: { 'authorization': "token " + token },
        dataType: "json",
        contentType: "application/json",
        async: true,
        cache: false,
        success: function (Object) { }
      });       
}

function checkNotPending(){
     var userid = window.atob(localStorage.getItem('uKey'));
     var token = localStorage.getItem('token'); 
    
     $.ajax({
        type: "GET",
        url: www+"checkNotPending?userid="+userid,
        headers: { 'authorization': "token " + token },
        dataType: "json",
        contentType: "application/json",
        async: true,
        cache: false,
        success: function (Object) {
            if(Object['status']==200){
               count_not = Object['count']; 
               changeCountNot(count_not);
            }else{
               count_not = 0;
               changeCountNot(count_not);
            }
        }
      });       
}

function changeCountNot(count){
    //globo flotante boton
    jQuery("#kendoUiMobileApp").find("div[id*='not_count_']").each(function(index,div){
        if(count>0){
            $(div).html(count);    
            $(div).show();    
        }else{
            $(div).hide();    
        } 
    });
    
    //globo flotante menu
    jQuery("#kendoUiMobileApp").find("p[id*='not_count_menu_']").each(function(index,div){
        if(count>0){
            $(div).html(count);    
            $(div).show();    
            $("#li_profile").attr('style','padding-bottom:10px;');
            $("#li_notpush").attr('style','padding-bottom:10px;');
        }else{
            $(div).hide();    
            $("#li_profile").removeAttr('style');
            $("#li_notpush").removeAttr('style');
        } 
    });
}

function goWebPay(){
    goMenu('myProfile','Perfil');
    app.mobileApp.navigate('components/webpay/view.html');
    app.webpay.onShow();
}

function checkPinPass(page){
    app.mobileApp.navigate('components/checkPinPass/view.html');
    localStorage.setItem("pagePinPass",page);
}

function checkPinPass2(page){
    if(page=='webpayInit'){
        goWebPay();
        setTimeout(function(){
            initOneClick();
        },500);
    }else if(page=='setCard'){
        goWebPay();
        setTimeout(function(){
            setCard2(localStorage.getItem("card_id"));
        },500);
    }else if(page=='deleteCard'){
        goWebPay();
        setTimeout(function(){
            deleteCard2(localStorage.getItem("card_id"));
        },500);
    }else if(page == 'sales_2'){
        app.mobileApp.navigate('components/sales_2/view.html');
        setTimeout(function(){
            finalBuy(localStorage.getItem("planid"),localStorage.getItem("money_plan"));
        },500);
    }else if(page == 'payments'){
        app.mobileApp.navigate('components/payments/view.html');
        setTimeout(function(){
            finalPayPending(localStorage.getItem("pending_id"),localStorage.getItem("money_pending"));
        },500);
    }else if(page != 'savePinPass'){
        goMenu('ptEnergy','PT Energy');
        app.mobileApp.navigate('components/'+page+'/view.html');
    }else{
        localStorage.setItem('actionBtnSavePinPass',1);
        app.mobileApp.navigate('components/savePinPass/view.html');
    }
}

function notPush(token,userid){
    if (window.navigator.simulator === true) {
    }else{
      //OneSignal Notifications Push

      var notificationOpenedCallback = function (jsonData) {
        localStorage.setItem("notid",jsonData.notification.payload.notificationID);
        checkNotPending();
        app.mobileApp.navigate('components/inbox_2/view.html');
        goMenu('myProfile','Perfil');
        app.inbox_2.onShow();
      };
        
      var notificationReceivedCallback = function () {
        navigator.vibrate(200);
        checkNotPending();
      };

      //para saber si es android o no :)
      var device = null;
      if (kendo.support.mobileOS.android) {
          
        //window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
          
        window.plugins.OneSignal
          .startInit("bbef5b1d-3652-42ae-bf04-5727497c17fb","695156040856")
          .handleNotificationOpened(notificationOpenedCallback)
          .handleNotificationReceived(notificationReceivedCallback)
          .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
          .endInit();
        device = 'Android';
      } else {
        var iosSettings = {};
        //FALTA EL ALERT DICIENDO PORQUE OTORGAR PERMISOS DE NOTIFICACIONES Y LANZAR ALERT LUEGO DE ACEPTAR EL PRIMERO
        iosSettings["kOSSettingsKeyAutoPrompt"] = true;
        iosSettings["kOSSettingsKeyInAppLaunchURL"] = true;

        //window.plugins.OneSignal.setLogLevel({ logLevel: 4, visualLevel: 4 });
          
        window.plugins.OneSignal
          .startInit("bbef5b1d-3652-42ae-bf04-5727497c17fb")
          .iOSSettings(iosSettings)
          .handleNotificationOpened(notificationOpenedCallback)
          .handleNotificationReceived(notificationReceivedCallback)
          .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
          .endInit();
        device = 'iOS';
      }
        
      //se registra el userIdPush y el pushToken relacionados al usuario que ingreso en la BD
      window.plugins.OneSignal
        .getIds(function (ids) {
                                    
          jQuery.ajax({
            type: "POST",
            url: www + "pushNotifications",
            headers: { 'authorization': "token " + token },
            data: "userid=" + userid + "&useridToken=" + ids.userId + "&pushToken=" + ids.pushToken + "&device=" + device + '&model=' + model_device + '&version_so=' + version_so + '&version_app=' + version_app,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            async: true,
            beforeSend: function (xhr) { },
            success: function (Object) {
              if (Object['status'] == 200) {
                //ver que pasara aqui con status 200
              } else {
                //ver que pasara aqui con status 404
              }
            },
            error: function (xhr, err) {
              //ver que pasara cuando tenga problemas de servidor
            }
          });
        });
      
      window.plugins.OneSignal.enableVibrate(true);
      window.plugins.OneSignal.enableSound(true);

      //OneSignal Notifications Push
   }
}

function goMyzone(){
    window.open("http://myzone.org/", "_system");
}

function goMyzone2(){
    if(cordova.platformId == 'android'){
        //android
        window.open("market://details?id=com.myzone.myzoneble&hl=es_419", "_system");
    }else{
        //ios   
        window.open("https://itunes.apple.com/cl/app/myzone/id969938732?mt=8", "_system");
    }
}

function updateDateApp(){  
    if (window.navigator.simulator === true) {
        
    }else{
      var token = localStorage.getItem('token'); 
      if(token != null){
         $.ajax({
            type: "GET",
            url: www+"updateDateApp",
            headers: { 'authorization': "token " + token },
            dataType: "json",
            contentType: "application/json",
            async: true,
            cache: false,
            success: function (Object) {}
          });         
      }   
    }
}

function checkVersion(){
    if (window.navigator.simulator === true) {
        
    }else{
        var platform = cordova.platformId;
        var force_update = null;
        var version_cloud = null;
        
        model_device = device.model;
        version_so = device.version;
        
         $.ajax({
            type: "GET",
            url: www+"getVersion?platform="+platform,
            headers: { 'authorization': "token " + token_admin },
            dataType: "json",
            contentType: "application/json",
            async: true,
            cache: false,
            success: function (Object) {
                if (Object['status'] == 200) {
                    version_cloud = Object['data']['version'];
                    force_update = Object['data']['force_update']; 
                    
                    var arr = [];
                    arr.push(version_app);
                    arr.push(version_cloud);
                    arr.sort();
                    if(arr[1] != version_app){
                        if(force_update == 'N'){
                            navAlert('Actualización Disponible',"Ya esta disponible la versión "+version_cloud+".");      
                        }else{
                            navigator.notification.confirm(
                             "Ya esta disponible la versión "+version_cloud+". Para poder continuar debes actualizar la aplicación.",
                               confirmUpdate,            
                              'Actualización Disponible',           
                              ['Actualizar']
                            );
                        }
                    }
                }
            }
          }); 
    }
}

function confirmClass(){
    if(cordova.platformId == 'android'){
        //android
        //window.open("market://details?id=com.ebrain.locampino", "_system");
    }else{
        //ios   
        //window.open("https://itunes.apple.com/cl/app/energy-valle-lo-campino/id1264750909?mt=8", "_system");
    }
}

function goInfoUser(){
    app.mobileApp.navigate('components/profile/view.html');
    goMenu('myProfile','Perfil');
}

function navAlert(title,text){
  navigator.notification.alert(
    text
    ,null
    ,title
    ,'Aceptar'
  );
}

function goHref(href){
    scrollMenu();
    app.mobileApp.navigate(href);
}

function goHomeButton(){
    app.mobileApp.navigate('components/home/view.html');
    backMainMenu();
}

function scrollMenu(){
    var listview = $("#navigation-container").data("kendoMobileListView");
    listview.scroller().reset();
}

function qrScanner(qr_url){
   //if (window.navigator.simulator === true) {
   //  navAlert('Aviso','Simulador NO Soporta BarcodeScanner');
   //}else{ 
   //    cordova.plugins.diagnostic.isLocationAvailable(function(available){
   //      if(available){
   //         navigator.geolocation.getCurrentPosition(function(position){
                
                cordova.plugins.barcodeScanner.scan(function(result) {
                  if (!result.cancelled) {
                    var userid = window.atob(localStorage.getItem('uKey'));
                    var qr = result.text.split("&");
                    var clientID = qr[0].split('=');
                    var qrcode = qr[1].split('=');
                    var latitude = 0;//position.coords.latitude;
                    var longitude = 0;//position.coords.longitude;
                        
                    $.ajax({
                      type: "POST",
                      cache: false,
                      data: { 'clientID': clientID[1], 'qr': qrcode[1], 'userid': userid, 'longitude': longitude, 'latitude': latitude },
                      url: qr_url,
                      async: true,
                      beforeSend: function (xhr){
                        app.mobileApp.pane.loader.show();
                      },
                      success: function (resp){
                        navigator.vibrate(200);
                        if(resp['data'] == 'OK'){
                          navAlert('Aviso','Acceso Autorizado!');      
                        }else{
                          navAlert('Aviso','Error de conexión con API!');      
                        }
                      },
                      error: function (xhr, err){
                        navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                      }
                    });
                    app.mobileApp.pane.loader.hide();
                  }
                }, 
                function(error) {
                    //error al tratar de leer qr
                    //navAlert('Error [3]: ' + error.code, error.message);
                });
                
   //         },function(error){
   //             //error al tratar de tomar la geolocalizacion actual
   //             navAlert('Error [2]: ' + error.code, error.message);
   //         },{maximumAge: 30000, timeout: 3000, enableHighAccuracy: true});    
   //      }else{
   //          navAlert('Aviso',"Para poder utilizar el código QR debes tener activado el GPS.");
   //      }
   //   }, function(error){
   //       //error al ver si el gps estaba activado
   //       navAlert('Error [1]: ' + error.code, error.message);
   //   });
   //}
}

function cleanRun(crut){
  var tmpstr = '';
  crut = crut.toLowerCase();
  for ( i=0; i < crut.length ; i++ )
    if ( crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-' ){
      tmpstr = tmpstr + crut.charAt(i);
    }
  return tmpstr;
}

function isset() {
    var a = arguments,
        l = a.length,
        i = 0,
        undef;

    if (l === 0) {
        throw new Error('Empty isset');
    }

    while (i !== l) {
        if (a[i] === undef || a[i] === null) {
            return false;
        }
        i++;
    }
    return true;
}

function substr(str, start, len) {
  var i = 0,
      allBMP = true,
      es = 0,
      el = 0,
      se = 0,
      ret = '';
  str += '';
  var end = str.length;

  this.php_js = this.php_js || {};
  this.php_js.ini = this.php_js.ini || {};
  switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
    case 'on':
      for (i = 0; i < str.length; i++) {
        if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
          allBMP = false;
          break;
        }
      }

      if (!allBMP) {
        if (start < 0) {
          for (i = end - 1, es = (start += end) ; i >= es; i--) {
            if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
              start--;
              es--;
            }
          }
        } else {
          var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
          while ((surrogatePairs.exec(str)) != null) {
            var li = surrogatePairs.lastIndex;
            if (li - 2 < start) {
              start++;
            } else {
              break;
            }
          }
        }

        if (start >= end || start < 0) {
          return false;
        }
        if (len < 0) {
          for (i = end - 1, el = (end += len) ; i >= el; i--) {
            if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
              end--;
              el--;
            }
          }
          if (start > end) {
            return false;
          }
          return str.slice(start, end);
        } else {
          se = start + len;
          for (i = start; i < se; i++) {
            ret += str.charAt(i);
            if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
              se++;
            }
          }
          return ret;
        }
        break;
      }
    case 'off':
    default:
      if (start < 0) {
        start += end;
      }
      end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
      return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
  }
  return undefined;
}

function dateFormat(dateObject) {
  date = new Date(dateObject + ' GMT');
  date.setMinutes(date.getMinutes() + date.getTimezoneOffset());
  return kendo.toString(date,'dd-MM-yyyy');
}

function dateNow() {
  var today = new Date(),
      dd = today.getDate(),
      mm = today.getMonth() + 1,
      yyyy = today.getFullYear();
  if (dd < 10) {
    if(substr(dd,0,1) != 0){
      dd = '0' + dd;
    }
  }
  if (mm < 10) {
    if(substr(mm,0,1) != 0){
      mm = '0' + mm;
    }
  }
  return yyyy + '-' + mm + '-' + dd;
}

function timeNow() {
  var today = new Date(),
      hour = today.getHours(),
      minutes = today.getMinutes();
  if (hour < 10) {
    if(substr(hour,0,1) != 0){
      hour = '0' + hour;
    }
  }
  if (minutes < 10) {
    if(substr(minutes,0,1) != 0){
      minutes = '0' + minutes;
    }
  }
  return hour+':'+minutes;
}

function dateTimeNow() {
  var now = dateNow() + ' ' + timeNow();
  return now;
}

function tableSlider(tableID) {
  jQuery("html,body").animate({ scrollTop: jQuery("." + tableID).offset().top - 50 }, 500);
}

function objectLength(obj) {
  var result = 0;
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      result++;
    }
  }
  return result;
}

function sha1(str) {
  var hash
  try {
    var crypto = require('crypto')
    var sha1sum = crypto.createHash('sha1')
    sha1sum.update(str)
    hash = sha1sum.digest('hex')
  } catch (e) {
    hash = undefined
  }

  if (hash !== undefined) {
    return hash
  }

  var _rotLeft = function (n, s) {
    var t4 = (n << s) | (n >>> (32 - s))
    return t4
  }

  var _cvtHex = function (val) {
    var str = ''
    var i
    var v

    for (i = 7; i >= 0; i--) {
      v = (val >>> (i * 4)) & 0x0f
      str += v.toString(16)
    }
    return str
  }

  var blockstart
  var i, j
  var W = new Array(80)
  var H0 = 0x67452301
  var H1 = 0xEFCDAB89
  var H2 = 0x98BADCFE
  var H3 = 0x10325476
  var H4 = 0xC3D2E1F0
  var A, B, C, D, E
  var temp

  // utf8_encode
  str = unescape(encodeURIComponent(str))
  var strLen = str.length

  var wordArray = []
  for (i = 0; i < strLen - 3; i += 4) {
    j = str.charCodeAt(i) << 24 |
      str.charCodeAt(i + 1) << 16 |
      str.charCodeAt(i + 2) << 8 |
      str.charCodeAt(i + 3)
    wordArray.push(j)
  }

  switch (strLen % 4) {
    case 0:
      i = 0x080000000
      break
    case 1:
      i = str.charCodeAt(strLen - 1) << 24 | 0x0800000
      break
    case 2:
      i = str.charCodeAt(strLen - 2) << 24 | str.charCodeAt(strLen - 1) << 16 | 0x08000
      break
    case 3:
      i = str.charCodeAt(strLen - 3) << 24 |
        str.charCodeAt(strLen - 2) << 16 |
        str.charCodeAt(strLen - 1) <<
      8 | 0x80
      break
  }

  wordArray.push(i)

  while ((wordArray.length % 16) !== 14) {
    wordArray.push(0)
  }

  wordArray.push(strLen >>> 29)
  wordArray.push((strLen << 3) & 0x0ffffffff)

  for (blockstart = 0; blockstart < wordArray.length; blockstart += 16) {
    for (i = 0; i < 16; i++) {
      W[i] = wordArray[blockstart + i]
    }
    for (i = 16; i <= 79; i++) {
      W[i] = _rotLeft(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1)
    }

    A = H0
    B = H1
    C = H2
    D = H3
    E = H4

    for (i = 0; i <= 19; i++) {
      temp = (_rotLeft(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff
      E = D
      D = C
      C = _rotLeft(B, 30)
      B = A
      A = temp
    }

    for (i = 20; i <= 39; i++) {
      temp = (_rotLeft(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff
      E = D
      D = C
      C = _rotLeft(B, 30)
      B = A
      A = temp
    }

    for (i = 40; i <= 59; i++) {
      temp = (_rotLeft(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff
      E = D
      D = C
      C = _rotLeft(B, 30)
      B = A
      A = temp
    }

    for (i = 60; i <= 79; i++) {
      temp = (_rotLeft(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff
      E = D
      D = C
      C = _rotLeft(B, 30)
      B = A
      A = temp
    }

    H0 = (H0 + A) & 0x0ffffffff
    H1 = (H1 + B) & 0x0ffffffff
    H2 = (H2 + C) & 0x0ffffffff
    H3 = (H3 + D) & 0x0ffffffff
    H4 = (H4 + E) & 0x0ffffffff
  }

  temp = _cvtHex(H0) + _cvtHex(H1) + _cvtHex(H2) + _cvtHex(H3) + _cvtHex(H4)
  return temp.toLowerCase()
}

//base 64

var Base64 = {
  // private property
  _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

  // public method for encoding
  encode : function (input) {
      var output = "";
      var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
      var i = 0;

      input = Base64._utf8_encode(input);

      while (i < input.length) {

          chr1 = input.charCodeAt(i++);
          chr2 = input.charCodeAt(i++);
          chr3 = input.charCodeAt(i++);

          enc1 = chr1 >> 2;
          enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
          enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
          enc4 = chr3 & 63;

          if (isNaN(chr2)) {
              enc3 = enc4 = 64;
          } else if (isNaN(chr3)) {
              enc4 = 64;
          }

          output = output +
          Base64._keyStr.charAt(enc1) + Base64._keyStr.charAt(enc2) +
          Base64._keyStr.charAt(enc3) + Base64._keyStr.charAt(enc4);

      }

      return output;
  },

  // public method for decoding
  decode : function (input) {
      var output = "";
      var chr1, chr2, chr3;
      var enc1, enc2, enc3, enc4;
      var i = 0;

      input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

      while (i < input.length) {

          enc1 = Base64._keyStr.indexOf(input.charAt(i++));
          enc2 = Base64._keyStr.indexOf(input.charAt(i++));
          enc3 = Base64._keyStr.indexOf(input.charAt(i++));
          enc4 = Base64._keyStr.indexOf(input.charAt(i++));

          chr1 = (enc1 << 2) | (enc2 >> 4);
          chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
          chr3 = ((enc3 & 3) << 6) | enc4;

          output = output + String.fromCharCode(chr1);

          if (enc3 != 64) {
              output = output + String.fromCharCode(chr2);
          }
          if (enc4 != 64) {
              output = output + String.fromCharCode(chr3);
          }

      }

      output = Base64._utf8_decode(output);

      return output;

  },

  // private method for UTF-8 encoding
  _utf8_encode : function (string) {
      string = string.replace(/\r\n/g,"\n");
      var utftext = "";

      for (var n = 0; n < string.length; n++) {

          var c = string.charCodeAt(n);

          if (c < 128) {
              utftext += String.fromCharCode(c);
          }
          else if((c > 127) && (c < 2048)) {
              utftext += String.fromCharCode((c >> 6) | 192);
              utftext += String.fromCharCode((c & 63) | 128);
          }
          else {
              utftext += String.fromCharCode((c >> 12) | 224);
              utftext += String.fromCharCode(((c >> 6) & 63) | 128);
              utftext += String.fromCharCode((c & 63) | 128);
          }

      }

      return utftext;
  },

  // private method for UTF-8 decoding
  _utf8_decode : function (utftext) {
      var string = "";
      var i = 0;
      var c = c1 = c2 = 0;

      while ( i < utftext.length ) {

          c = utftext.charCodeAt(i);

          if (c < 128) {
              string += String.fromCharCode(c);
              i++;
          }
          else if((c > 191) && (c < 224)) {
              c2 = utftext.charCodeAt(i+1);
              string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
              i += 2;
          }
          else {
              c2 = utftext.charCodeAt(i+1);
              c3 = utftext.charCodeAt(i+2);
              string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
              i += 3;
          }

      }
      return string;
  }
}

function checkRut(textrut){
  var tmpstr = "";
  var intlargo = textrut;
  if (intlargo.length> 0)
  {
    crut = textrut;
    largo = crut.length;
    if ( largo <2 )
    {
      //alert('Rut inválido');
      //Objeto.focus();
      return false;
    }
    for ( i=0; i <crut.length ; i++ )
    if ( crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-' )
    {
      tmpstr = tmpstr + crut.charAt(i);
    }
    rut = tmpstr;
    crut=tmpstr;
    largo = crut.length;
  
    if ( largo> 2 )
      rut = crut.substring(0, largo - 1);
    else
      rut = crut.charAt(0);
  
    dv = crut.charAt(largo-1);
  
    if ( rut == null || dv == null )
    return 0;
  
    var dvr = '0';
    suma = 0;
    mul  = 2;
  
    for (i= rut.length-1 ; i>= 0; i--)
    {
      suma = suma + rut.charAt(i) * mul;
      if (mul == 7)
        mul = 2;
      else
        mul++;
    }
  
    res = suma % 11;
    if (res==1)
      dvr = 'k';
    else if (res==0)
      dvr = '0';
    else
    {
      dvi = 11-res;
      dvr = dvi + "";
    }
  
    if ( dvr != dv.toLowerCase() )
    {
      //alert(':: INCORRECTO :: Un R.U.N Ingresado no es válido');
      //Objeto.focus();
      return false;
    }
    //if(alertOk == true) alert(':: CORRECTO :: El R.U.N Ingresado es valido!');
    return true;
  } else {
    /*alert("No existe R.U.N que validar.");
    Objeto.focus();*/
    return true;
  }
}