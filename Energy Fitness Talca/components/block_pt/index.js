'use strict';

app.block_pt = kendo.observable({
    afterShow: function() {},
    onShow: function() {
          var name_user = localStorage.getItem('name_user');
          var photo = localStorage.getItem('photo_user');
          
          $("#name_menu").html('&nbsp;'+name_user);            
          if (photo && substr(photo, 0, 4) == 'data'){
             $("#photo_menu").attr('src',photo);       
          }   
          
          changeCountNot(count_not);
        
          goMenu('ptEnergy','PT Energy');
        
          var token = localStorage.getItem('token'), 
              userid = window.atob(localStorage.getItem('uKey'));
          
          $.ajax({
            type: "GET",
            url: www+"getClubsPTSell?userid=" + userid,
            headers: { 'authorization': 'token ' + token },
            dataType: "json",
            contentType: "application/json",
            async: true,
            cache: false,
            beforeSend: function (xhr) {
                app.mobileApp.pane.loader.show();
                $("#div_block_pt").hide();
                $("#errorBlockPT").hide();
            },
            success: function (Object) {
                if (Object['status'] == 200) {
                    var obj = Object['data'];
                    var data = [];
                    //var content = '';
                    for (var index in obj) {
                        data.push({ text: obj[index]['name'], value: obj[index]['id'] });
                        
                        var template = kendo.template($("#template_dates_block").html());
                        var previewHtml = template({clubid:obj[index]['id']});
                        $("#formBlockPT").html($("#formBlockPT").html()+previewHtml);
                    }
                                    
                    $("#clubid_blockpt").kendoDropDownList({
                      dataTextField: "text",
                      dataValueField: "value",
                      valueTemplate: '<span style="color:white;">#: data.text #</span>',
                      dataSource: data,
                    });
                    
                    //$("#div_blockpt_show").html(content);
                    $("#div_block_pt").show();
                    
                    
                    for (var index2 in obj) {
                        var data_temp = [];
                        for (var index3 in obj[index2]['time_today_start']) {
                            var field = obj[index2]['time_today_start'][index3];
                            data_temp.push({ text: field, value: field });    
                        }
                        $("#date_start_today_" + obj[index2]['id']).kendoDropDownList({
                          dataTextField: "text",
                          dataValueField: "value",
                          dataSource: data_temp,
                        });
                        
                        var data_temp = [];
                        for (var index3 in obj[index2]['time_today_end']) {
                            var field = obj[index2]['time_today_end'][index3];
                            data_temp.push({ text: field, value: field });    
                        }
                        $("#date_end_today_" + obj[index2]['id']).kendoDropDownList({
                          dataTextField: "text",
                          dataValueField: "value",
                          dataSource: data_temp,
                        });
                        
                        var data_temp = [];
                        for (var index3 in obj[index2]['time_tomorrow_start']) {
                            var field = obj[index2]['time_tomorrow_start'][index3];
                            data_temp.push({ text: field, value: field });    
                        }
                        $("#date_start_tomorrow_" + obj[index2]['id']).kendoDropDownList({
                          dataTextField: "text",
                          dataValueField: "value",
                          dataSource: data_temp,
                        });
                        
                        var data_temp = [];
                        for (var index3 in obj[index2]['time_tomorrow_end']) {
                            var field = obj[index2]['time_tomorrow_end'][index3];
                            data_temp.push({ text: field, value: field });    
                        }
                        $("#date_end_tomorrow_" + obj[index2]['id']).kendoDropDownList({
                          dataTextField: "text",
                          dataValueField: "value",
                          dataSource: data_temp,
                        });
                    }
                    
                    showDay();
                  
                } else {                
                    $("#h1ErrorBlockPT").html('Ha ocurrido un error,<br>por favor volver a intentar.');  
                    $("#btnBlockPTHome").hide();      
                    $("#btnBlockPTReload").show();    
                    $("#errorBlockPT").show();
                }
                app.mobileApp.pane.loader.hide();
            },
            error: function (xhr, err) {
                app.mobileApp.pane.loader.hide();
                $("#h1ErrorBlockPT").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
                $("#btnBlockPTHome").hide();      
                $("#btnBlockPTReload").show();    
                $("#errorBlockPT").show();
            }
          });
     },
});

function showDay(){
    var clubid = $("#clubid_blockpt").val();
    var type = $('input:radio[name=day]:checked').val();
    type = type == 1 ? 'today' : 'tomorrow';
    
    
    $('#formBlockPT').find("li[id*='div_block_']").each(function(index,div){        
        $(div).attr('style','display:none;');
        
    });
    
    $("#li_blockpt_show").show();
    $("#div_block_"+type+"_1_"+clubid).show();
    $("#div_block_"+type+"_2_"+clubid).show();
    $("#div_block_button_" + clubid).attr('style','text-align:center;');
}

function activatePT(){
    var clubid = $("#clubid_blockpt").val();
    var type = $('input:radio[name=day]:checked').val();
    type = type == 1 ? 'today' : 'tomorrow';
    
    var start = $("#date_start_"+type+"_"+clubid).val();
    var end = $("#date_end_"+type+"_"+clubid).val();
    
    var splitStart = start.split(':');
    var splitEnd = end.split(':');
    
    if(start < end){
        if(splitStart[0]!=splitEnd[0]){
            var diff_hour = parseInt(splitEnd[0]) - parseInt(splitStart[0]);
            var pass = true;
            var point = '';
            if(diff_hour==1){
              if( splitStart[1] == '15' ){
                  if( splitEnd[1] == '00'){
                      pass = false;
                      point = '[2]';
                  }
              }else if( splitStart[1] == '30' ){
                  if( parseInt(splitEnd[1]) < 30){
                      pass = false;
                      point = '[3]';
                  }
              }else if( splitStart[1] == '45' ){
                  if( parseInt(splitEnd[1]) < 45){
                      pass = false;
                      point = '[4]';
                  }
              }
            }
            
            if(pass){
                
                navigator.notification.confirm(
                  '¿Activar bloque en las horas seleccionadas?\nRecuerda que puedes Activar/Desactivar este bloque en "Historial Disponibilidad".',
                   confirmBlock,            
                  'Confirmar',           
                  ['Confirmar','Cancelar']
                );
                
                function confirmBlock(indexB){
                    if(indexB == 1){
                        var token = localStorage.getItem('token'), 
                          userid = window.atob(localStorage.getItem('uKey'));
                        $.ajax({
                          type: "POST",
                          url: www+"saveBlockPT",
                          headers: { 'authorization': "token " + token },
                          dataType: "json",
                          data: "userid=" + userid + "&clubid=" + clubid + "&type=" + type+ "&start=" + start+ "&end=" + end,
                          cache: false,
                          async: true,
                          beforeSend: function (xhr) {
                            app.mobileApp.pane.loader.show();
                          },
                          success: function (Object) {
                            if (Object['status'] == 200) {
                              navigator.vibrate(200);
                              navAlert("Aviso", '¡Bloque activado!\nRecuerda que puedes Activar/Desactivar este bloque en "Historial Disponibilidad".');
                              app.block_pt.onShow();
                              app.mobileApp.scroller().reset();
                            } else {
                              if (Object['error'] == 'ERROR-TAKED') {
                                navAlert('Error', 'Usted ya tiene un bloque dentro de las horas seleccionadas. ['+Object['data']['time']+']');
                              }else if (Object['error'] == 'ERROR-NOT-PRODUCT') {
                                navAlert('Error', 'Usted NO tiene ningun producto asociado para vender.. favor contactar a su administrador.');
                              } else {
                                navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                              }
                            }
                            app.mobileApp.pane.loader.hide();
                          },
                          error: function (xhr, err) {
                            navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                            app.mobileApp.pane.loader.hide();
                          }
                        });       
                    }
                }

            }else{
                navAlert('Aviso', 'Debe existir una diferencia de al menos una hora para poder activar el bloque. ' + point);
            }
        }else{
            navAlert('Aviso', 'Debe existir una diferencia de al menos una hora para poder activar el bloque. [1]');
        }
    }else{
        navAlert('Aviso', 'Fecha término bloque debe ser mayor a la fecha de inicio.');
    }    
}        