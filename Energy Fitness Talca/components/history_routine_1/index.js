'use strict';

app.history_routine_1 = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('myRoutine','Mi Rutina');
      
    localStorage.removeItem("evalid");
    localStorage.removeItem("date_routine");
      
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www+"historyRoutine?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#div_history_routine_1").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
                        
          var routine = Object['data'];
          var data = [];  
            
          for(var count_temp in routine){
            for (var id in routine[count_temp]) {
              data.push({ evalid: id, date: routine[count_temp][id]['date'], n_machine: routine[count_temp][id]['n_machine'] });
            }
          }
                        
          $("#div_history_routine_1").kendoListView({
            template: kendo.template($("#template_history_routine_1").html()),
            dataSource: data ,
          });
          $("#div_history_routine_1").show();
                     
        } else {
          $("#h1ErrorHistoryRoutine").html('NO tienes rutinas<br>disponibles.');  
          $("#btnHistoryRoutineHome").show();      
          $("#btnHistoryRoutineReload").hide();    
          $("#errorHistoryRoutine").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorHistoryRoutine").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnHistoryRoutineHome").hide();      
        $("#btnHistoryRoutineReload").show();    
        $("#errorHistoryRoutine").show();
      }
    });
  app.mobileApp.scroller().reset();      
  }
});

//function que redirecciona a la vista de una rutina
function seeRoutine(id,date){
  localStorage.setItem("evalid",id);
  localStorage.setItem("date_routine",date);
  app.mobileApp.navigate("components/history_routine_2/view.html");
  
}