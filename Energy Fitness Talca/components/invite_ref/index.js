'use strict';

app.invite_ref = kendo.observable({
    afterShow: function() {},
    onShow: function() {
        
      var token = localStorage.getItem('token'), 
          userid = userid = window.atob(localStorage.getItem('uKey'));
        
      //name and photo user
      var name_user = localStorage.getItem('name_user');
      $("#name_menu").html('&nbsp;'+name_user);            
      var photo = localStorage.getItem('photo_user');
      if (photo && substr(photo, 0, 4) == 'data'){
         $("#photo_menu").attr('src',photo);       
      }
        
      changeCountNot(count_not);
        
      backMainMenu();
        
    /*clean vars*/
        $('#club_invite').val(''); 
        $('#userid_invite').val('');
        $('#days_freepass_granted').val('');
        $('#name_invite').val(''); 
        $('#lastn_invite').val(''); 
        $('#email_invite').val(''); 
        $('#cellphone_invite').val(''); 
    /*clean vars*/

    $.ajax({
      type: "GET",
      url: www+"canRefer?userid=" + userid,
      headers: { 'authorization': 'token ' + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#total_invite").hide();
        $("#myinvite").hide();
        $("#errorInvite").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var obj = Object['data'];
          var data = [];
          for (var index in obj['club']) {
            data.push({ text: obj['club'][index], value: index });
          }
            
          $("#club_invite").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            valueTemplate: '<span style="color:white;">#: data.text #</span>',
            dataSource: data,
          });
            
          var dataDays = [];
          for (var index_days in obj['days_invite']) {
            dataDays.push({ text: obj['days_invite'][index_days], value: index_days });
          }
            
          $("#days_freepass_granted").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            valueTemplate: '<span style="color:white;">#: data.text #</span>',
            dataSource: dataDays,
          });
            
          var msg = '';
          if(obj['total'] == -1){
            msg+= "<b>"+obj['n_invite']['used']+" invitaciones enviadas este mes.</b>";
          }else{
            msg+= "<b>"+obj['n_invite']['used']+"/"+obj['n_invite']['total']+" invitaciones enviadas este mes.</b>";
          }  
            
          $("#total_invite").html(msg);
          $("#total_invite").show();
          $("#myinvite").show();
          $("#li_invite").show();
        } else {
          $("#li_invite").hide();
          $("#h1ErrorInvite").html('Debes tener una membresía activa, para poder hacer invitaciones a amigos.');  
          $("#btnInviteHome").show();      
          $("#btnInviteReload").hide();    
          $("#errorInvite").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorInvite").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnInviteHome").hide();      
        $("#btnInviteReload").show();    
        $("#errorInvite").show();
      }
    });
    app.mobileApp.scroller().reset();
    }
});

function sendInvite(){
  var userid_invite = $("#userid_invite").val();
  var name_invite = $("#name_invite").val();
  var lastn_invite = $("#lastn_invite").val();
  var email_invite = $("#email_invite").val();
  var cellphone_invite = $("#cellphone_invite").val();
  var club_invite = $("#club_invite").val();
  var days_freepass_granted = $("#days_freepass_granted").val();
      
  var bool_email = false;
  if (email_invite != '') {
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (emailRegex.test(email_invite)) {
      bool_email = true;
    }
  } 
  
  var bool_empty = false;
  if(name_invite != '' && lastn_invite != '' && email_invite != '' && cellphone_invite != ''){
    bool_empty = true;
  }  
  
  var bool_cellphone = false;  
  if(cellphone_invite.length == 9){
      bool_cellphone = true;  
  }
    
  var bool_rut = true;
  if(userid_invite !=''){
    if(!checkRut(userid_invite)){
        bool_rut = false;
    }      
  }
  
  if(bool_empty){
    if (bool_email) {
        if(bool_cellphone){
            if(bool_rut){
               cellphone_invite = '+56' + cellphone_invite;
               navigator.notification.confirm(
                  "¿Enviar invitación?",
                   onConfirmInvite,            
                  'Confirmar',           
                  ['Confirmar','Cancelar']
                );
                  
                function onConfirmInvite(indexB){
                    if(indexB == 1){
                        var token = localStorage.getItem('token'), 
                        userid = userid = window.atob(localStorage.getItem('uKey'));  
                    
                        $.ajax({
                          type: "POST",
                          url: www + "sendInvRef",
                          headers: { 'authorization': "token " + token },
                          data: "userid=" + userid + "&email_invite=" + email_invite + "&cellphone_invite=" + cellphone_invite + "&club_invite=" + club_invite + "&name_invite=" + name_invite+ "&lastn_invite=" + lastn_invite + "&userid_invite=" + userid_invite + "&days_freepass_granted=" + days_freepass_granted + "&name_app=" + name_app,
                          dataType: "json",
                          contentType: "application/json",
                          cache: false,
                          async: true,
                          beforeSend: function (xhr) {
                            app.mobileApp.pane.loader.show();
                          },
                          success: function (Object) {
                            if (Object['status'] == 200) {
                              navigator.vibrate(200);
                              navAlert("Aviso", 'Invitación Enviada!');  
                              setTimeout(function(){
                                goHomeButton();
                              },1500);
                            }else if(Object['status'] == 201){//si entra aca es porque debe elegir a uno de la lista que viene
                              var dataUsers = Object['data'];//array with users
                              var data = [];
                              for(var index_users in dataUsers){
                                  data.push({ userid: index_users, name: dataUsers[index_users]});   
                              }
                                
                              $("#ul_invite_options").kendoListView({
                                template: kendo.template($("#template_invite_options").html()),
                                dataSource: data ,
                              });  
                                
                              $("#modalViewInvite").kendoWindow({
                                title: "Seleccione..",
                                modal: true,
                                draggable: false,
                                width: "90%"  
                              }).data("kendoWindow").center().open();
                                
                            } else if(Object['error'] == 'ERROR-INVITATION-FULL'){
                              navAlert("Error", 'Usted NO cuenta con mas invitaciones por este mes. Favor intentar en un nuevo período.');
                            } else if(Object['error'] == 'ERROR-MAIL-WAIT'){
                              navAlert("Error", 'La invitación no ha podido ser enviada, porque la persona "'+Object['data']['used_by']+'", con el correo "' + Object['data']['email'] + '", ya ha aceptado una invitación en este club. Favor intentar enviar la invitación pasando la fecha ' + Object['data']['nextTake'] + '.');
                            } else if(Object['error'] == 'ERROR-SAME-USERID'){
                              navAlert("Error", 'La invitación no ha podido ser enviada, porque el R.U.N ingresado debe ser distinto al suyo.');
                            } else {
                              navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                            }
                            app.mobileApp.pane.loader.hide();
                          },
                          error: function (xhr, err) {
                            app.mobileApp.pane.loader.hide();
                            navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                          }
                        });
                    }
                }      
            }else{
                navAlert('Aviso', 'RUT ingresado no es válido.');
            }
        }else{
            navAlert('Aviso', 'Número de teléfono debe contener 9 dígitos.');
        }
    } else {
      navAlert('Aviso', 'Debe ingresar un correo electrónico correcto.');
    }
  }else{
    navAlert("Aviso","Debes completar el formulario para continuar.");
  }
}

function sendInvite2(){
    if($("input[name='userid_temp']:radio").is(':checked')){
      var new_user = $("input[name='userid_temp']:checked").val();
      $("#userid_invite").val(new_user);
      sendInvite();
      $("#modalViewInvite").data("kendoWindow").close();    
    }else{
       navAlert('Aviso','Completar Formulario.');       
    }
}

function cancelModal(){
  $("#modalViewInvite").data("kendoWindow").close();    
}