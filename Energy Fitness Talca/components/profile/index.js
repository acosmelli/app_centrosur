'use strict';
var old_cellphone, old_email = null;

app.profile = kendo.observable({
    afterShow: function() {},
    onShow: function () {
      //name and photo user
      var name_user = localStorage.getItem('name_user');
      $("#name_menu").html('&nbsp;'+name_user);            
      var photo = localStorage.getItem('photo_user');
      if (photo && substr(photo, 0, 4) == 'data'){
         $("#photo_menu").attr('src',photo);       
      }
        
      changeCountNot(count_not);
        
      goMenu('myProfile','Perfil');
        
      var token = localStorage.getItem('token'); 
      var userid = window.atob(localStorage.getItem('uKey'));
        
      $.ajax({
        type: "GET",
        url: www+"userInfo?userid=" + userid,
        headers: { 'authorization': "token " + token },
        dataType: "json",
        contentType: "application/json",                    
        async: true,
        beforeSend: function (xhr) {
          $("#myprofile").hide();
          $("#errorProfile").hide();
          app.mobileApp.pane.loader.show();
        },
        success: function (Object) {
          if (Object['status'] == 200) {
            var img = "resources/no-photo.jpg",
                obj = Object['data'];
              
            var photo = localStorage.getItem('photo_user');
            if (photo && substr(photo, 0, 4) == 'data') { img = photo; }
            
            $("#imgprofile").attr('src',img);
            $("#name").val(obj['name']);
            $("#lastn").val(obj['lastn']);
            $("#lastnm").val(obj['lastnm']);
            $("#birthday").val(obj['birthday']);
            $("#new_cellphone").val(obj['cellphone']);
            old_cellphone = obj['cellphone'];
            $("#new_email").val(obj['email']);
            old_email = obj['email'];
              
            $("#myprofile").show();
          }else{
            $("#h1ErrorProfile").html('Ha ocurrido un error, por <br>favor volver a intentar.');  
            $("#btnProfileHome").hide();      
            $("#btnProfileReload").show();    
            $("#errorProfile").show();
          }
          app.mobileApp.pane.loader.hide();
        },
        error: function (xhr, err) {
          app.mobileApp.pane.loader.hide();
          $("#h1ErrorProfile").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
          $("#btnProfileHome").hide();      
          $("#btnProfileReload").show();    
          $("#errorProfile").show();
        }
      });
      app.mobileApp.scroller().reset();
    }
});

function changePhoto(){
    if (window.navigator.simulator === true) {
     navAlert('Aviso','Simulador NO Soporta Camara');
   }else{
     navigator.camera.getPicture(
        function(imgData) {
            var token = localStorage.getItem('token'); 
            var userid = window.atob(localStorage.getItem('uKey'));
            
            var src_img = "data:image/jpeg;base64,"+imgData;
            
            //ajax para guardar aca en servidor y tambien en disposivito en actualizar variable de la imagen
            $.ajax({
                type: "POST",
                url: www + "updatePhoto",
                headers: { 'authorization': "token " + token },
                data: "userid=" + userid + "&src_img=" + src_img,
                dataType: "json",
                contentType: "application/json",
                cache: false,
                async: true,
                beforeSend: function (xhr) {
                  app.mobileApp.pane.loader.show();
                },
                success: function (Object) {
                  if (Object['status'] == 200) {
                    navigator.vibrate(200);
                    navAlert("Aviso", 'Foto de perfil actualizada!');
                      
                    //una vez terminado se vuelve a cargar la vista de perfil
                    localStorage.setItem("photo_user",src_img);
                    setTimeout(window.location.reload(true), 2000);
  
                  } else {
                    navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                  }
                  app.mobileApp.pane.loader.hide();
                },
                error: function (xhr, err) {
                  app.mobileApp.pane.loader.hide();
                  navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                }
              });
        },
        function() {
            //error or cancel
        },
        {   quality: 100,
            destinationType: Camera.DestinationType.DATA_URL,//que sea un string
            sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Album
            encodingType: 0,    // 0=JPG 1=PNG --> si esta en png se pierde su orientacion.. leer sobre esto
            correctOrientation: true,
            cameraDirection: 1,//0 trasera . 1 selfie
            targetHeight :320,
            targetWidth  :320,
            allowEdit: true // para recortar la foto y dejar en el tamaño que yo quiero 
        }
     );
   }
}

function sendContact() {
  var new_email = $("#new_email").val();
  var new_cellphone = $("#new_cellphone").val();
  //validate cellphone  and email.. que al menos venga uno lleganos
  var bool_email = false;
  if (new_email != '') {
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (emailRegex.test(new_email)) {
      bool_email = true;
    }
  }
  var bool_same = true;
  if(new_email != old_email || new_cellphone != old_cellphone){
    bool_same = false;
  }
    
  var bool_cellphone = true;  
  if(new_cellphone != ''){
    if(new_cellphone.length != 9){
        bool_cellphone = false;  
    }      
  }
    
  var token = localStorage.getItem('token'); 
  var userid = window.atob(localStorage.getItem('uKey'));
    
  //aca trabajar enviando los datos a editar !!!
  if (!bool_same) {
    if (bool_email) {
        if(bool_cellphone){
           //new_cellphone = '+56' + new_cellphone;
           navigator.notification.confirm(
            "¿Editar información de contacto?",
             onChangeProfile,            
            'Confirmar',           
            ['Confirmar','Cancelar']
          );
            
          function onChangeProfile(indexB){         
              if(indexB == 1){
                $.ajax({
                  type: "POST",
                  url: www + "editContact",
                  headers: { 'authorization': "token " + token },
                  data: "userid=" + userid + "&new_email=" + new_email + "&new_cellphone=" + new_cellphone,
                  dataType: "json",
                  contentType: "application/json",
                  cache: false,
                  async: true,
                  beforeSend: function (xhr) {
                    app.mobileApp.pane.loader.show();
                  },
                  success: function (Object) {
                    if (Object['status'] == 200) {
                      navigator.vibrate(200);
                      navAlert("Aviso", 'Datos de contacto actualizado!');
                      setTimeout(function(){
                        app.profile.onShow();
                        app.mobileApp.scroller().reset();
                      },1500);
                    } else {
                      navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                    }
                    app.mobileApp.pane.loader.hide();
                  },
                  error: function (xhr, err) {
                    app.mobileApp.pane.loader.hide();
                    navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                  }
                });    
              }
          }   
        }else{
            navAlert('Aviso', 'Número de teléfono debe contener 9 dígitos.');
        }
    } else {
      navAlert('Aviso', 'Debe ingresar un correo electrónico correcto.');
    }
  } else {
    navAlert('Aviso', 'Datos de contacto ya actualizados.');
  }
}