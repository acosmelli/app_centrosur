'use strict';

app.payments = kendo.observable({
  afterShow: function () { },
  onShow: function () {  
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('myProfile','Perfil');
      
    //localStorage.removeItem("pending_id");
    //localStorage.removeItem("money_pending");
      
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www+"getMyCharges?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      beforeSend: function (xhr) {
        $("#div_payments").hide();
        $("#errorPayments").hide();
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var chN = Object['data']['N'],
              chP = Object['data']['P'],
              ar_pending = [],
              ar_paid = [],
              div = "";
          if (isset(chP)) {
            div += "<h2 style='text-align:center;color:white;'>Cargos Pendientes</h2>";
            div += "<div id='pendingGrid'></div><hr style=\"border:1px solid grey;\">";
            var count =0;
            for(var index in chP){
              var alink = '<button id="btn_pending_'+count+'" class="km-button km-primary" onclick="payPending(\''+index+'\',\''+chP[index]['totalpay']+'\',\''+chP[index]['total']+'\')">Pagar</button>';
              ar_pending.push({ charge: chP[index]['charge'], date: dateFormat(chP[index]['date']), total: chP[index]['total'], link: alink });
              count++;
            }
          }
          if (isset(chN)) {
            div += "<h2 style='text-align:center;color:white;'>Boletas Pagadas</h2>";
            div += "<div id='paidGrid'></div>";
            for (var index in chN) {
              var alink = "<div style=\"text-align:center;\"><a href='" + chN[index]['link'] + "' ><i style='color:black;' class='fa fa-cloud-download fa-2x' aria-hidden='true'></i></a></div>";
              ar_paid.push({ paperid: chN[index]['paperid'], datepaid: chN[index]['datepaid'], total: chN[index]['total'], link: alink });
            }
          }
          if(div != ''){
            $("#div_payments").html(div);
            $("#div_payments").show();
            $("#pendingGrid").kendoGrid({
              dataSource: { data: ar_pending, pageSize: 5 },
              pageable: { buttonCount: 2 },
              scrollable: false,
              sortable: true,
              columns: [
                { field: "charge", title: "Cargo", width: "40%", encoded: false },
                { field: "date", title: "F. Pago", width: "30%", encoded: false },
                { field: "total", title: "Valor", width: "20%", encoded: false },
                { field: "link", title: "&nbsp", width: "10%", encoded: false }
              ]
            });
            $("#paidGrid").kendoGrid({
              dataSource: { data: ar_paid, pageSize: 5 },//data --> arreglo con datos, cantidad de info por pagina
              pageable: { buttonCount: 2 }, //pageable: lanza paginacion .. buttoncount: setea el maximo a ver de n en select
              scrollable: false,//obliga al grid a tener separaciones
              sortable: true,//ordenar grid por fila
              columns: [
                { field: "paperid", title: "Boleta", width: "30%", encoded: false },
                { field: "datepaid", title: "F. Pago", width: "30%", encoded: false },
                { field: "total", title: "Valor", width: "20%", encoded: false },
                { field: "link", title: "&nbsp", width: "20%", encoded: false }
              ]
            });
              
            //solo se habilita el primer boton de los cargos (esto solo aplica si existen cargos pendientes por el rut)
            $("#div_payments").find("button[id*='btn_pending_']").each(function(index,btn){
                var id = $(btn).attr('id');
                var split = id.split('_');
                if(split[2] != 0){
                   $(btn).attr("disabled", true);
                   $(btn).attr("style","background:grey;border-color:grey");
                }
            });
              
          }else{
            $("#h1ErrorPayments").html('NO tienes cargos<br>disponibles.');  
            $("#btnPaymentsHome").show();      
            $("#btnPaymentsReload").hide();    
            $("#errorPayments").show();
          }
        } else {
          $("#h1ErrorPayments").html('NO tienes cargos<br>disponibles.');  
          $("#btnPaymentsHome").show();      
          $("#btnPaymentsReload").hide();    
          $("#errorPayments").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorPayments").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnPaymentsHome").hide();      
        $("#btnPaymentsReload").show();    
        $("#errorPayments").show();
      }
    });
  app.mobileApp.scroller().reset();      
  }
});

function payPending(id,money,money_text){
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www+"getMyCreditCards?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {},
      success: function (Object) {
          if (Object['status'] == 200) {     
              //aca ver lo del descuento!
              localStorage.setItem("pending_id",id);
              localStorage.setItem("money_pending",money);
              navigator.notification.confirm(
                "La cuota pendiente a pagar tiene un valor de $"+money_text+". ¿Desea continuar?",
                 onPay,
                'Pagar Pendiente',           
                ['Cancelar','Pagar']
              );                       
          } else {
              navigator.notification.confirm(
               "Para poder pagar la cuota pendiente, debes tener al menos una tarjeta de crédito registrada!",
                 goWebPay,            
                'Importante',           
                ['Agregar']
              );
          }
          app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        //navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');        
      }
    });
}

function onPay(indexB){
    if(indexB == 2){
      checkPinPass('payments');
    }
}

function finalPayPending(id,money){
    var token = localStorage.getItem('token');
    var userid = window.atob(localStorage.getItem('uKey'));
       
    $.ajax({
      type: "POST",
      url: www+"payPending",
      headers: { 'authorization': "token " + token },
      dataType: "json",
      data: "userid=" + userid + "&pendingid=" + id + '&money='+money,
      cache: false,
      async: true,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
          if (Object['status'] == 200) {
              navigator.vibrate(200);
              navAlert("Aviso", '¡Pago cuota pendiente ha sido aceptado!'); 
              app.payments.onShow();
              app.mobileApp.scroller().reset();
          } else {
              if(Object['error'] == 'ERROR-MONEY-DAY-MAX'){//máximo monto diario de pago excedido
                  navAlert('Error', 'Usted ha excedido el monto máximo diario a utilizar en su tarjeta de crédito.');
              }else if(Object['error'] == 'ERROR-MONEY-EXCEEDED'){//máximo monto de pago excedido
                  navAlert('Error', 'Usted ha excedido el monto máximo a utilizar en su tarjeta de crédito.');
              }else if(Object['error'] == 'ERROR-MONEY-COUNT'){//máxima cantidad de pagos diarios excedido
                  navAlert('Error', 'Usted ha excedido los pagos diarios a realizar en su tarjeta de crédito.');
              }else{
                  navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
              }
          }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        navAlert('Error: ' + xhr.status + ' / ' + err, 'Problemas de conectividad, intente mas tarde...');
        app.mobileApp.pane.loader.hide();
      }
    });  
}