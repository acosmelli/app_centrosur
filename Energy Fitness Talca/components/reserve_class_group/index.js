'use strict';

app.reserve_class_group = kendo.observable({
  afterShow: function () { },
  onShow: function () {
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('classGroup','Clases Grupales');
      
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));

    $.ajax({
      type: "GET",
      url: www + "canEnterToGym?userid=" + userid,
      headers: { 'authorization': 'token ' + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#div_reserve_class").hide();
        $("#div_class").hide();
        $("#errorReserveClass").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var obj = Object['data'];
          var data = [];
            
          for (var index in obj['clubs']) {
            data.push({ text: obj['clubs'][index], value: index });
          }  
            
          var div = "<div id='club_reserve_cg'><label style='color:white;'>Club a Reservar</label><input onchange='loadClasses(this.value,false,"+data.length+")' id='clubid_class' style='width:100%;'/></div>";
          div += "<label style='color:white;'>Día a Reservar</label><input placeholder='Todos los días..' class='input-date' id='reserve_date_class' value='' style='width:100%;'/>";
          div += "<div id='div_search' style='display:none;'><label style='color:white;'>Nombre Clase</label><input id='name_class' style='width:100%;'/><br><br>";
          div += "<div align='center'><button onclick='searchReserveClass()' style='width: 40%;' class='km-button km-primary' id='search_class'><i class='fa fa-search fa-lg' aria-hidden='true'></i> Buscar</button>";
          div += "<button onclick='cleanFormClassG()' style='width: 40%;' class='km-button' id='clean_class'><i class='fa fa-refresh fa-lg' aria-hidden='true'></i> Limpiar</button></div></div>";  

          $("#div_reserve_class").html(div);
          $("#div_reserve_class").show();
          $("#reserve_date_class").kendoDatePicker({
            format: "dd-MM-yyyy",
            culture: "es-CL",
            disableDates: function (date) {
              var today = new Date();
              if (kendo.toString(new Date(date), 'yyyy-MM-dd') < kendo.toString(new Date(today), 'yyyy-MM-dd')) {
                  return true;
              } else {
                  return false;
              }
            },
            footer: false,
          });
          $("#reserve_date_class").attr("readonly", true);
          if(data.length > 1){
            $("#clubid_class").kendoDropDownList({
              dataTextField: "text",
              dataValueField: "value",
              dataSource: data,
              /*optionLabel: 'Seleccione..',
              index: 1*/
            });    
          }else{
            $("#club_reserve_cg").attr('style','display:none;');
            $("#clubid_class").val(data[0].value);
          }
          
          if(obj['can_refer'] == true){
            $("#li_invite").show();    
          }else{
            $("#li_invite").hide();    
          }
          loadClasses(data[0].value, true,data.length);
        } else {
          $("#div_reserve_class").hide();
          $("#div_class").hide();
          $("#li_invite").hide();
            
          $("#h1ErrorReserveClass").html('Usted NO cuenta<br>con acceso al gimnasio,<br>para poder reservar clases<br>grupales con nuestros profesores.');  
          $("#btnReserveClassHome").hide();      
          $("#btnReserveClassReload").show();    
          $("#errorReserveClass").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorReserveClass").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnReserveClassHome").hide();      
        $("#btnReserveClassReload").show();    
        $("#errorReserveClass").show();
      }
    });
   app.mobileApp.scroller().reset();
  }
});

function cleanFormClassG(){
    $("#reserve_date_class").val('');
    $("#name_class").data("kendoDropDownList").value("");
    $("#div_class").hide();
    $("#clubid_class").data("kendoDropDownList").select(0);
}

function loadClasses(clubid, first,n_clubs) {
  var token = localStorage.getItem('token');
  $.ajax({
    type: "GET",
    url: www + "getNameClassGroupByClub?clubid=" + clubid,
    headers: { 'authorization': 'token ' + token },
    dataType: "json",
    contentType: "application/json",
    async: true,
    cache: false,
    beforeSend: function (xhr) {
      app.mobileApp.pane.loader.show();
      $("#errorReserveClass").hide();
    },
    success: function (Object) {
      if (Object['status'] == 200) {
        var obj = Object['data'];
        var data = [];
        for (var index in obj) {
          data.push({ text: obj[index], value: index });
        }

        $("#name_class").kendoDropDownList({
          dataTextField: "text",
          dataValueField: "value",
          dataSource: data,
          optionLabel: 'Todas las clases..',
          //optionLabelTemplate:'<span style="background-color:yellow">Todas las clases..</span>',
        });
        $("#div_search").show();
      } else {
        if(n_clubs == 1){
          $("#h1ErrorReserveClass").html('<br>Club sin reservas disponibles de clases grupales. favor intentar mas tarde..');  
        }else{
          $("#h1ErrorReserveClass").html('<br>Club sin reservas disponibles de clases grupales. favor intentar seleccionando otro club o intente mas tarde..');
        }
        $("#btnReserveClassHome").hide();      
        $("#btnReserveClassReload").show();    
        $("#errorReserveClass").show();
      }
      app.mobileApp.pane.loader.hide();
    },
    error: function (xhr, err) {
      app.mobileApp.pane.loader.hide();
      $("#h1ErrorReserveClass").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
      $("#btnReserveClassHome").hide();      
      $("#btnReserveClassReload").show();    
      $("#errorReserveClass").show();
    }
  });
}

function searchReserveClass() {
  var token = localStorage.getItem('token'), 
      userid = window.atob(localStorage.getItem('uKey')),
      clubid = jQuery("#clubid_class").val(),
      date = jQuery("#reserve_date_class").val(),
      class_configid = jQuery("#name_class").val();
  $("#div_class").hide();

  $.ajax({
    type: "GET",
    url: www + "getGroupClassByDate?date=" + date + "&clubid=" + clubid + "&userid=" + userid + "&class_configid=" + class_configid,
    headers: { 'authorization': "token " + token },
    dataType: "json",
    contentType: "application/json",
    async: true,
    cache: false,
    beforeSend: function (xhr) {
      app.mobileApp.pane.loader.show();
      $("#errorReserveClass").hide();
    },
    success: function (Object) {
      if (Object['status'] == 200) {
        var obj = Object['data'];
        var main = "";
        var count_temp = 0;
          
        for (var index in obj) {
          var classG = obj[index];
          var quota_total_online = classG['quota_total_online'] - classG['quota_full_online'];            
          var div = "<div class='panel panel-default'><div class='panel-body'><table class='table table-condensed'>";
                      
          div += '<tr><td colspan="2" style="text-align:center"><button class="km-button" onclick="seeInfo('+count_temp+')"><i class="fa fa-info-circle" aria-hidden="true"></i> ' + classG['name'] + '</button></td></tr>';
            
          div += "<tr><td>Profesor</td><td>" + classG['proffesor'] + "</td></tr>";
          div += "<tr><td>Día</td><td>" + classG['date'] + "</td></tr>";
          div += "<tr><td>Hora</td><td>" + classG['time'] + " - " + classG['time_end'] + "</td></tr>";
            
          if(classG['spivi'] == 'Y'){
              div += "<tr><td>Asientos Disponibles</td><td>" + quota_total_online + " / " + parseInt(classG['quota_total_online']-2) + "</td></tr>";
          }else{
              div += "<tr><td>Cupos Online</td><td>" + quota_total_online + " / " + classG['quota_total_online'] + "</td></tr>";
          }
            
          div += "<tr><td>Término Inscripción</td><td>" + classG['closeIns'] + "</td></tr>";
              
          if(classG['used'] == false){
            if (quota_total_online == 0) {
              div += "<tr><td colspan='2' style='text-align:center;'><b>Inscripciones Online Completas!</b></td></tr>";
            } else {
              if(classG['spivi'] == 'Y'){
                div += "<tr><td colspan='2' style='text-align:center;'><button style='width:80%;' class='km-button km-primary' onclick='takeSpivi(" + classG['id'] + ",\"" + classG['name'] + "\",\"" + classG['proffesor'] + "\",\"" + classG['time'] + "\",\"" + classG['date'] + "\"," + clubid + ",\"" + classG['clubname'] + "\",\"" + classG['time_end'] + "\")'><i class='fa fa-check fa-lg' aria-hidden='true'></i> Reservar Cupo</button></td></tr>";      
              }else{
                div += "<tr><td colspan='2' style='text-align:center;'><button style='width:80%;' class='km-button km-primary' onclick='takeClass(" + classG['id'] + ",\"" + classG['name'] + "\",\"" + classG['proffesor'] + "\",\"" + classG['time'] + "\",\"" + classG['date'] + "\"," + clubid + ")'><i class='fa fa-check fa-lg' aria-hidden='true'></i> Reservar Cupo</button></td></tr>";    
              }              
            }
          }else{
            div += "<tr><td colspan='2' style='text-align:center;'><b>Clase Reservada!</b></td></tr>";
          }
          div += "</table></div></div>";
          
          //contenido modal window informacion clase grupal
          div += "<div id='descClass"+count_temp+"' style='display:none'>";
          div += "<table style='color:black;font-size:11px;' class='table table-condensed'><tbody>";
          div += "<tr><td style='width:40%'>Nombre Clase</td><td style='width:60%'>" + classG['name'] + "</td></tr>";
          div += "<tr><td style='width:40%'>Descripción</td><td style='width:60%'>" + classG['desc'] + "</td></tr>";
          div += "<tr><td style='width:40%'>Condiciones de uso</td><td style='width:60%'>" + classG['usecondition'] + "</td></tr>";
          div += "<tr><td style='width:40%'>Tiempo previo para comenzar inscripciones</td><td style='width:60%'>" + classG['timeOpen'] + "</td></tr>";
          div += "<tr><td style='width:40%'>Tiempo posterior para cerrar inscripciones</td><td style='width:60%'>" + classG['timeClose'] + "</td></tr>";
          if (classG['access'] == 'Y') {
            div += "<tr><td style='width:40%'>Tiempo previo al Inicio de la clase para liberar cupos</td><td style='width:60%'>" + classG['freequota'] + "</td></tr>";
            div += "<tr><td style='width:40%'>Tiempo posterior al inicio de la clase para aplicar no-show</td><td style='width:60%'>" + classG['noshow'] + "</td></tr>";
            div += "<tr><td style='width:40%'>Tiempo de castigo por no-show</td><td style='width:60%'>" + classG['punishNoshow'] + "</td></tr>";
          }
          div += "</tbody></table>";
          div += "</div>";
                                                
          main += div;  
          count_temp++;
        }
        
        $("#div_class").html('<hr style="border-color:#292929">' + main);
        $("#div_class").show();

      } else {
        $("#h1ErrorReserveClass").html('<br>Club sin reservas disponibles para la fecha seleccionada, por favor intente seleccionando otra fecha.');
        $("#btnReserveClassHome").hide();      
        $("#btnReserveClassReload").hide();    
        $("#errorReserveClass").show();
      }
      app.mobileApp.pane.loader.hide();
    },
    error: function (xhr, err) {
      navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
      app.mobileApp.pane.loader.hide();
    }
  });
}

function takeSpivi(classid, classname, profname, time, date, clubid, clubname, time_end) {
  localStorage.setItem("classid_cg",classid);
  localStorage.setItem("classname_cg",classname);
  localStorage.setItem("profname_cg",profname);
  localStorage.setItem("time_cg",time);
  localStorage.setItem("time_end_cg",time_end);
  localStorage.setItem("date_cg",date);
  localStorage.setItem("clubid_cg",clubid);
  localStorage.setItem("clubname_cg",clubname);
  app.mobileApp.navigate("components/reserve_spivi/view.html");
}

function seeInfo(count){
    $("#descClass"+count).kendoWindow({
      title: "Información Adicional",
      modal: true,
      draggable: false,
      width: "90%"  
    }).data("kendoWindow").center().open();
}

function takeClass(classid, classname, profname, time, date, clubid) {
    navigator.notification.confirm(
      "¿Reservar cupo para la clase " + classname + " el día " + date + " a las " + time + " horas, con el profesor " + profname + "?",
       confirmClass,            
      'Confirmar',           
      ['Confirmar','Cancelar']
    );
    
    function confirmClass(indexB){
        if(indexB == 1){
            var token = localStorage.getItem('token'), 
              userid = window.atob(localStorage.getItem('uKey'));
            $.ajax({
              type: "POST",
              url: www+"reserveClass",
              headers: { 'authorization': "token " + token },
              dataType: "json",
              data: "userid=" + userid + "&classid=" + classid + "&clubid=" + clubid,
              cache: false,
              async: true,
              beforeSend: function (xhr) {
                app.mobileApp.pane.loader.show();
              },
              success: function (Object) {
                app.mobileApp.pane.loader.hide();
                if (Object['status'] == 200) {
                  navigator.vibrate(200);
                  navAlert("Aviso", 'Clase reservada!');
                  app.reserve_class_group.onShow();
                  app.mobileApp.scroller().reset();
                } else {
                  if (Object['error'] == 'ERROR-USER-TAKED') {
                    navAlert('Error', "Usted ya se encuentra registrado en esta clase.");
                  } else if (Object['error'] == 'ERROR-QUOTA-FULL') {
                    navAlert('Error', "Todos los cupos online han sido ocupados.");
                  } else if (Object['error'] == 'ERROR-NO-SHOW') {
                    navAlert('Error', "Usted registra un castigo por No Show al no haber asistido a la clase: " + Object['data']['classname'] + ", desde " + Object['data']['since'] + " hasta " + Object['data']['until'] + ".");
                  } else if (Object['error'] == 'ERROR-OTHER-CLASS') {
                    navAlert('Error', "Usted ya registra una clase en este horario.");
                  } else {
                    navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                  }
                }
              },
              error: function (xhr, err) {
                navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                app.mobileApp.pane.loader.hide();
              }
            });       
        }
    }
}