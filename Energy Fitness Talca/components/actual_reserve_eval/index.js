'use strict';

app.actual_reserve_eval = kendo.observable({
  afterShow: function () {},
  onShow: function () {
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('evaluations','Evaluaciones');
      
    var token = localStorage.getItem('token'), 
        userid = window.atob(localStorage.getItem('uKey'));

    $.ajax({
      type: "GET",
      url: www + "getMyFutureReservesEval?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        $("#div_myReserve").hide();
        $("#errorActualReserveEval").hide();
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var obj = Object['data'],
              div = '';
          for (var id in obj) {
            var roomptid = obj[id];
            div += "<div class='panel panel-default'><div class='panel-body'><table class='table table-condensed'>";
            div += "<tr><td>Club</td><td>" + roomptid['club_name'] + "</td></tr>";
            div += "<tr><td>Sala</td><td>" + roomptid['room_name'] + "</td></tr>";
            div += "<tr><td>Profesor</td><td>" + roomptid['ptname'] + "</td></tr>";
            div += "<tr><td>Fecha</td><td>" + roomptid['date'] + ' ' + roomptid['hour'] + "</td></tr>";
            div += "<tr><td colspan='2'><div align='center'><button type='button' style='width:80%;' class='km-button km-primary' href='#' onclick='cancelReserve(" + roomptid['id'] + ",\"" + roomptid['ptname'] + "\",\"" + roomptid['date'] + "\",\"" + roomptid['hour'] + "\")' ><i class='fa fa-times fa-lg' aria-hidden='true'></i> Cancelar Reserva</button></div></td></tr>";
            div += "</table></div></div>";
          }
          $("#div_myReserve").html("<br>" + div);
          $("#div_myReserve").show();
        } else {
          $("#h1ErrorActualReserveEval").html('NO tienes reservas<br>futuras de evaluaciones.');  
          $("#btnActualReserveEvalHome").show();      
          $("#btnActualReserveEvalReload").hide();    
          $("#errorActualReserveEval").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorActualReserveEval").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnActualReserveEvalHome").hide();      
        $("#btnActualReserveEvalReload").show();    
        $("#errorActualReserveEval").show();
      }
    });
   app.mobileApp.scroller().reset();
  }
});

function cancelReserve(id,ptname,date,time) {
    navigator.notification.confirm(
      "¿Cancelar reserva evaluación para el día " + date + " a las " + time + " horas, con el profesor " + ptname + "?",
       onDeleteEval,            
      'Confirmar',           
      ['Confirmar','Cancelar']
    );
    
    function onDeleteEval(indexB){
          if(indexB == 1){
              var token = localStorage.getItem('token');

              $.ajax({
                type: "POST",
                url: www+"deleteEvaluation",
                headers: { 'authorization': "token " + token },
                dataType: "json",
                data: "roomptid=" + id,
                cache: false,
                async: true,
                beforeSend: function (xhr) {
                  app.mobileApp.pane.loader.show();
                },
                success: function (Object) {
                  if (Object['status'] == 200) {
                    navigator.vibrate(200);
                    app.actual_reserve_eval.onShow();
                  } else {
                    navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                  }
                  app.mobileApp.pane.loader.hide();
                  
                },
                error: function (xhr, err) {
                  app.mobileApp.navigate('components/home/view.html');
                  app.mobileApp.pane.loader.hide();
                  backMainMenu('evaluations');
                }
              });
          }
    }
}