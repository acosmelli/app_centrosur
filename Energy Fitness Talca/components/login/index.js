'use strict';

app.login = kendo.observable({
  afterShow: function() {},
  onShow: function () {
    
    var userid = localStorage.getItem('uKey');
    if(userid != null){
       userid = window.atob(localStorage.getItem('uKey'));
       var pass = window.atob(localStorage.getItem('pKey')),
           base64User = window.btoa(userid+':'+pass);  
      $.ajax({
        type: "POST",
        cache: false,
        url: www,
        headers: { 'authorization': 'eBrainAuth1.0 ' + base64User },
        dataType: "json",
        contentType: "application/json",                    
        async: true,
        beforeSend: function (xhr) {
          app.mobileApp.pane.loader.show();
        },
        success: function (Object) {
          if(Object['status'] == 200){
            var token = Object['data']['token'],
              username = Object['data']['username'],
              photo = Object['data']['photo'],
              can_refer = Object['data']['can_refer'],
              isPT = Object['data']['isPT'],
              force_change = Object['data']['force_change'];
              
            localStorage.clear();  
            sessionStorage.clear();
            
            localStorage.setItem("token",token);
            localStorage.setItem("uKey",window.btoa(userid));  
            localStorage.setItem("pKey", window.btoa(pass));
            
            //name and photo
            localStorage.setItem("name_user", username);
            localStorage.setItem("photo_user",photo);
            
            //si la contraseña es temporal o no  
            localStorage.setItem("force_change",force_change);
              
            //name and photo user
            var name_user = localStorage.getItem('name_user');
            $("#name_menu").html('&nbsp;'+name_user);            
            var photo = localStorage.getItem('photo_user');
            if (photo && substr(photo, 0, 4) == 'data'){
               $("#photo_menu").attr('src',photo);       
            }
            
            if(can_refer == false){
                $("#li_invite").hide();      
            }
              
            if(isPT == false){
                $("#ptEnergy1").hide();      
                $("#ptEnergy2").hide();      
                $("#ptEnergy3").hide();        
            }  
              
            checkNotPending();
            notPush(token,userid);
            
            if(force_change == 'N'){
              app.mobileApp.navigate('components/home/view.html');    
            }else{
              app.mobileApp.navigate('components/changePassword/view.html');    
            }

            navigator.vibrate(200);
            app.mobileApp.scroller().reset();
          } else {
            $("#userid").val('');
            $("#password").val('');
            setTimeout(function() {
              $("#div_signin").fadeIn(1500);
            },500);
            navAlert('Error', 'R.U.N y/o contraseña incorrectas.');
            localStorage.clear();  
            sessionStorage.clear(); 
          }
          app.mobileApp.pane.loader.hide();
        },
        error: function (xhr, err) {
          navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
          app.mobileApp.pane.loader.hide();
          $("#userid").val('');
          $("#password").val('');
          localStorage.clear();  
          sessionStorage.clear();  
          setTimeout(function() {
            $("#div_signin").fadeIn(1500);
          },500);
        }
      });
    }else{
      $("#userid").val('');
      $("#password").val('');
      setTimeout(function() {
        $("#div_signin").fadeIn(1500);
      },500);
    }
  app.mobileApp.scroller().reset();
  }
});

function login() {
  var userid = cleanRun(jQuery("#userid").val()),
      pass = jQuery("#password").val();
  if(pass != '' && userid != ''){
    pass = pass.replace(" ", "");
    var base64User = window.btoa(userid+':'+pass);
    $.ajax({
      type: "POST",
      cache: false,
      url: www,
      headers: { 'authorization': 'eBrainAuth1.0 ' + base64User },
      dataType: "json",
      contentType: "application/json",                    
      async: true,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var token = Object['data']['token'],
              username = Object['data']['username'],
              photo = Object['data']['photo'],
              can_refer = Object['data']['can_refer'],
              isPT = Object['data']['isPT'],
              force_change = Object['data']['force_change'];
          
          localStorage.clear();  
          sessionStorage.clear();
            
          localStorage.setItem("token",token);
          localStorage.setItem("uKey",window.btoa(userid));  
          localStorage.setItem("pKey", window.btoa(pass));
          
          //name and photo
          localStorage.setItem("name_user", username);
          localStorage.setItem("photo_user",photo);
            
          //si la contraseña es temporal o no  
          localStorage.setItem("force_change",force_change);
            
          if(can_refer == false){
              $("#li_invite").hide();      
          }
            
          if(isPT == false){
              $("#ptEnergy1").hide();      
              $("#ptEnergy2").hide();      
              $("#ptEnergy3").hide();        
          }  
          
          checkNotPending();
          notPush(token,userid);

          if(force_change == 'N'){
            app.mobileApp.navigate('components/home/view.html');    
          }else{
            app.mobileApp.navigate('components/changePassword/view.html');    
          }
            
          navigator.vibrate(200); 
          app.mobileApp.scroller().reset();
        } else {
          navAlert('Error', 'R.U.N y/o contraseña incorrectas.');
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
        app.mobileApp.pane.loader.hide();
      }
    });
  } else {
    navAlert('Aviso', 'Ingrese R.U.N y/o contraseña.');
  }
}