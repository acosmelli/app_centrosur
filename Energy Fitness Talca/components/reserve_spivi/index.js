'use strict';

app.reserve_spivi = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
      //name and photo user
      var name_user = localStorage.getItem('name_user');
      $("#name_menu").html('&nbsp;'+name_user);            
      var photo = localStorage.getItem('photo_user');
      if (photo && substr(photo, 0, 4) == 'data'){
         $("#photo_menu").attr('src',photo);       
      }
      
      changeCountNot(count_not);
      
      goMenu('classGroup','Clases Grupales');
        
      var classid = localStorage.getItem("classid_cg");
      var clubid = localStorage.getItem("clubid_cg");

      var classname = localStorage.getItem("classname_cg");
      var profname = localStorage.getItem("profname_cg");
      var time = localStorage.getItem("time_cg");
      var time_end = localStorage.getItem("time_end_cg");
      var date = localStorage.getItem("date_cg");
      var clubname = localStorage.getItem("clubname_cg");
      
      $("#classname_sp").html(classname);
      $("#club_sp").html('Club: ' + clubname);
      $("#datetime_sp").html('Fecha: ' + date + ' ' + time + ' - ' + time_end);
      $("#profname_sp").html('Profesor: ' + profname);

      /*consulta en api fb para api spivi para ver asientos disponibles*/
      var token = localStorage.getItem('token');
      $.ajax({
        type: "GET",
        url: www + "getSeats?classid=" + classid + '&clubid=' + clubid,
        headers: { 'authorization': 'token ' + token },
        dataType: "json",
        contentType: "application/json",
        async: true,
        cache: false,
        beforeSend: function (xhr) {
          $("#seats").hide();
          app.mobileApp.pane.loader.show();
        },
        success: function (Object) {
          if (Object['status'] == 200) {            
            var objSeats = Object['seats'];
            //nueva forma con botones por silla y fila
            var div = '<div class="container row text-center">';
              
            for(var seatId in objSeats){
                if(seatId != 27 && seatId != 28){
                  div+= '<div class="col-xs-2 col-md-2 col-lg-2" style="margin:3px;">';    
                  if(objSeats[seatId] == 0){
                      div+= '<div id="seat_'+seatId+'" onclick="setSeatId('+seatId+')" class="btnSeats">'+seatId+'</div>';    
                  }else{
                      div+= '<div class="btnSeats" style="background-color:#ccc;">'+seatId+'</div>';
                  }
                  div+= '</div>';    
                }
                
            }
            div+= '</div>';
              
            $("#btn_seats").html(div);
            $("#seats").show();
            
          } else {
            navAlert('Aviso', 'Clase grupal ' + classname + ' sin asientos diponibles.');  
            app.mobileApp.navigate('components/reserve_class_group/view.html');
            app.reserve_class_group.onShow();
          }
          app.mobileApp.pane.loader.hide();
        },
        error: function (xhr, err) {
          app.mobileApp.pane.loader.hide();
          navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
          app.mobileApp.navigate('components/reserve_class_group/view.html');
          app.reserve_class_group.onShow();
        }
      });
      
      app.mobileApp.scroller().reset();      
  }
});

function setSeatId(seatid){
    $('#btn_seats').find("div[id*='seat_']").each(function(index,div){        
        if( $(div).attr('id') == 'seat_'+seatid ){
            $(div).attr('style','background-color:red;');        
        }else{
            $(div).attr('style','background-color:green;');
        }
    });

    $('#seat_id').val(seatid);
}

function reserveSpivi(){
    var seatid = $("#seat_id").val();
    if(seatid != 0){
        
        var classname = localStorage.getItem("classname_cg");
        var profname = localStorage.getItem("profname_cg");
        var time = localStorage.getItem("time_cg");
        var date = localStorage.getItem("date_cg");
        var clubname = localStorage.getItem("clubname_cg");
                
        navigator.notification.confirm(
          "¿Reservar cupo para la clase " + classname + " el día " + date + " a las " + time + " horas, con el profesor " + profname + "?",
           confirmClassSpivi,            
          'Confirmar',           
          ['Confirmar','Cancelar']
        );
    }else{
        navAlert('Aviso', 'Debe seleccionar un asiento para reservar esta clase.');  
    }
}

function confirmClassSpivi(indexB){
  if(indexB == 1){
      
      var seatid = $("#seat_id").val();
      var token = localStorage.getItem('token');
      var userid = window.atob(localStorage.getItem('uKey'));
      var classid = localStorage.getItem("classid_cg");
      var clubid = localStorage.getItem("clubid_cg");
      
      $.ajax({
        type: "POST",
        url: www+"reserveClass",
        headers: { 'authorization': "token " + token },
        dataType: "json",
        data: "userid=" + userid + "&classid=" + classid + "&clubid=" + clubid + '&seatid=' + seatid,
        cache: false,
        async: true,
        beforeSend: function (xhr) {
          app.mobileApp.pane.loader.show();
        },
        success: function (Object) {
          app.mobileApp.pane.loader.hide();
          if (Object['status'] == 200) {
            navigator.vibrate(200);
            navAlert("Aviso", 'Clase reservada!');
            setTimeout(function(){
              app.mobileApp.navigate('components/reserve_class_group/view.html')
            },1500);
          } else {
            if (Object['error'] == 'ERROR-USER-TAKED') {
              navAlert('Error', "Usted ya se encuentra registrado en esta clase.");
            } else if (Object['error'] == 'ERROR-QUOTA-FULL') {
              navAlert('Error', "Todos los cupos online han sido ocupados.");
            } else if (Object['error'] == 'ERROR-NO-SHOW') {
              navAlert('Error', "Usted registra un castigo por No Show al no haber asistido a la clase: " + Object['data']['classname'] + ", desde " + Object['data']['since'] + " hasta " + Object['data']['until'] + ".");
            } else if (Object['error'] == 'ERROR-OTHER-CLASS') {
              navAlert('Error', "Usted ya registra una clase en este horario.");
            } else if (Object['error'] == 'ERROR-NOT-EMAIL') {
              navAlert('Error', "Debe registrar un correo electrónico para poder reservar en esta clase.");
            } else {
              navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
            }
          }
        },
        error: function (xhr, err) {
          navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
          app.mobileApp.pane.loader.hide();
        }
      });       
  }
}