'use strict';

app.inbox = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('myProfile','Perfil');
      
    localStorage.removeItem("notid");
      
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www+"getAllNotificationsGeneric?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#div_inbox").hide();
        $("#errorInbox").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
                        
          var notification = Object['data'];
          var data = [];  
            
          for(var count in notification){
            var message = window.JSON.parse(notification[count]['message']);
            var title = window.JSON.parse(notification[count]['title']);
             data.push({ id: notification[count]['id'], message: message, title: title, status: notification[count]['status'], date: notification[count]['date'] });
          }
                        
          $("#div_inbox").kendoListView({
            template: kendo.template($("#template_inbox").html()),
            dataSource: data ,
          });
          $("#div_inbox").show();           
        } else {
          $("#h1ErrorInbox").html('NO tienes mensajes<br>disponibles.');  
          $("#btnInboxHome").hide();      
          $("#btnInboxReload").show();    
          $("#errorInbox").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorInbox").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnInboxHome").hide();      
        $("#btnInboxReload").show();    
        $("#errorInbox").show();
      }
    });
  app.mobileApp.scroller().reset();      
  }
});

//function que redirecciona a la vista de una notificacion
function seeNot(id){
  localStorage.setItem("notid",id);
  app.mobileApp.navigate("components/inbox_2/view.html");
}

function deleteNot(id){
    navigator.notification.confirm(
      "¿Eliminar Notificación?",
       confirmDelete,            
      'Confirmar',           
      ['Confirmar','Cancelar']
    );
    
    function confirmDelete(indexB){
        if(indexB == 1){
            var token = localStorage.getItem('token');
            $.ajax({
              type: "POST",
              url: www+"deleteNotifications",
              headers: { 'authorization': "token " + token },
              dataType: "json",
              data: "notid=" + id,
              cache: false,
              async: true,
              beforeSend: function (xhr) {
                app.mobileApp.pane.loader.show();
              },
              success: function (Object) {
                app.mobileApp.pane.loader.hide();
                if (Object['status'] == 200) {
                  checkNotPending();
                  navigator.vibrate(200); 
                  app.inbox.onShow();
                }else{
                  navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                }
              },
              error: function (xhr, err) {
                navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                app.mobileApp.pane.loader.hide();
              }
            });       
        }
    }
}