'use strict';

app.actual_routine_1 = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('myRoutine','Mi Rutina');
      
    localStorage.removeItem("routineid");
    localStorage.removeItem("name_machine");
      
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www+"getRoutine?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#div_title_ar1").hide();
        $("#div_actual_routine_1").hide();
        $("#div_comment_ar1").hide();
        $("#errorActualRoutine").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
            
          //funcion para descargar la rutina completa en PDF  
          function downloadRoutine() {
            window.open(Object['data']['link'], '_system');
          }
                      
          var title = "<div align='center'><button class='km-button km-primary' id='downloadRoutine' style='text-transform: capitalize;'><i class='fa fa-cloud-download fa-lg' aria-hidden='true'></i> Descargar Rutina " + Object['data']['date'] + "</button></div><br>";
          $("#div_title_ar1").html(title);            
          $("#div_title_ar1").show();
          $("#downloadRoutine").kendoButton({ click: downloadRoutine });
            
          var machine = Object['data']['machine'];
          var data = [];
          var count = 1;  
          for (var id in machine) {
            var img = null;
            if (machine[id]['photo1'] != '') {
              img = machine[id]['photo1'];
            }else{
              img = '../resources/no-image.png';
            }
            data.push({ routineid: id, img: img, name: machine[id]['name'], count: count });
            count++;
          }
                        
          $("#div_actual_routine_1").kendoListView({
            template: kendo.template($("#template_actual_routine_1").html()),
            dataSource: data ,
          });
          $("#div_actual_routine_1").show();
          
          var comment = "<br><table class='table table-condensed'><thead><b style='color:white;'>Comentario General</b></thead><tbody>";
          comment += "<tr><td style='color:white;'>" + Object['data']['comment']+"</td></tr>";
          comment += "</tbody></table>";
            
          $("#div_comment_ar1").html(comment);
          $("#div_comment_ar1").show();
           
        } else {
          $("#h1ErrorActualRoutine").html('NO tienes rutinas<br>disponibles.');  
          $("#btnActualRoutineHome").show();      
          $("#btnActualRoutineReload").hide();    
          $("#errorActualRoutine").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorActualRoutine").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnActualRoutineHome").hide();      
        $("#btnActualRoutineReload").show();    
        $("#errorActualRoutine").show();
      }
    });
  app.mobileApp.scroller().reset();      
  }
});

//function que redirecciona a la vista por maquina o ejercicio
function seeMachine(id,name,count){
  localStorage.setItem("routineid",id);
  localStorage.setItem("name_machine",name);
  localStorage.setItem("count_machine",count);
  app.mobileApp.navigate("components/actual_routine_2/view.html");
  
}