'use strict';

app.actual_reserve_class = kendo.observable({
  afterShow: function () { },
  onShow: function () {
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('classGroup','Clases Grupales');
      
    var token = localStorage.getItem('token'), 
        userid = window.atob(localStorage.getItem('uKey'));

    $.ajax({
      type: "GET",
      url: www + "getMyFututeReservesClass?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        $("#div_list_reserve_class").hide();
        $("#errorActualReserveClass").hide();
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var obj = Object['data'],
              div = '<br>';
          for (var id in obj) {
            var enrollid = obj[id];
            div += "<div class='panel panel-default'><div class='panel-body'><table class='table table-condensed'>";
            div += "<tr><td>Club</td><td>" + enrollid['club'] + "</td></tr>";
            div += "<tr><td>Sala</td><td>" + enrollid['room_name'] + "</td></tr>";
            div += "<tr><td>Clase</td><td>" + enrollid['name'] + "</td></tr>";
            div += "<tr><td>Profesor</td><td>" + enrollid['proff'] + "</td></tr>";
            div += "<tr><td>Fecha</td><td>" + enrollid['date'] + ' ' + enrollid['hour'] + "</td></tr>";
            if(enrollid['seatid'] > 0){
                div += "<tr><td>Nº Asiento</td><td>" + enrollid['seatid'] + "</td></tr>";
            }
            div += "<tr><td colspan='2'><div align='center'><button type='button' style='width:80%;' class='km-button km-primary' href='#' onclick='cancelReserveClass(" + enrollid['id'] + ",\"" + enrollid['proff'] + "\",\"" + enrollid['date'] + "\",\"" + enrollid['hour'] + "\",\"" + enrollid['name'] + "\")' ><i class='fa fa-times fa-lg' aria-hidden='true'></i> Cancelar Reserva</button></div></td></tr>";
            div += "</table></div></div>";
          }
          $("#div_list_reserve_class").html(div);
          $("#div_list_reserve_class").show();
        } else {
          $("#h1ErrorActualReserveClass").html('NO tienes reservas<br>futuras de clases grupales.');  
          $("#btnActualReserveClassHome").show();      
          $("#btnActualReserveClassReload").hide();    
          $("#errorActualReserveClass").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorActualReserveClass").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnActualReserveClassHome").hide();      
        $("#btnActualReserveClassReload").show();    
        $("#errorActualReserveClass").show();
      }
    });
   app.mobileApp.scroller().reset();
  }
});

function cancelReserveClass(id, proff, date, time, name) {
    navigator.notification.confirm(
      "¿Cancelar reserva clase grupal " + name + " para el día " + date + " a las " + time + " horas, con el profesor " + proff + "?",
       onDeleteCG,            
      'Confirmar',           
      ['Confirmar','Cancelar']
    );
    
    function onDeleteCG(indexB){
        if(indexB == 1){
            var token = localStorage.getItem('token');
            $.ajax({
              type: "POST",
              url: www + "deleteClassGroup",
              headers: { 'authorization': "token " + token },
              dataType: "json",
              data: "enrollid=" + id,
              cache: false,
              async: true,
              beforeSend: function (xhr) {
                app.mobileApp.pane.loader.show();
              },
              success: function (Object) {
                if (Object['status'] == 200) {
                  navigator.vibrate(200);
                  app.actual_reserve_class.onShow();
                } else {
                  navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                }
                app.mobileApp.pane.loader.hide();
              },
              error: function (xhr, err) {
                app.mobileApp.pane.loader.hide();
                navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
              }
            });
        }
    }
}