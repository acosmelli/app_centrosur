'use strict';

app.sales = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    var token = localStorage.getItem('token');
    var userid = window.atob(localStorage.getItem('uKey'));
      
    $.ajax({
      type: "GET",
      url: www+"canBuy?userid="+userid,
      headers: { 'authorization': 'token ' + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
          $("#errorSales").hide();
          $("#div_sales").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
            
            if(Object['can'] == true){
                
                localStorage.setItem('type_sales',Object['type']);
                localStorage.setItem('date_start_sales',Object['date_start']);
                localStorage.setItem('date_start_text_sales',Object['date_start_text']);
                
                $.ajax({
                  type: "GET",
                  url: www+"getAllClubs",
                  headers: { 'authorization': 'token ' + token },
                  dataType: "json",
                  contentType: "application/json",
                  async: true,
                  cache: false,
                  beforeSend: function (xhr) {
                    app.mobileApp.pane.loader.show();
                      $("#errorSales").hide();
                  },
                  success: function (Object) {
                    if (Object['status'] == 200) {
                        var obj = Object['data'];
                        var data = [];
                        for (var index in obj['schedule']) {
                          data.push({ clubid: index, name: obj['clubs'][index] });
                        }             
                        
                        $("#div_btn_sales").kendoListView({
                          template: kendo.template($("#template_sales").html()),
                          dataSource: data ,
                        });
                        
                        $("#div_sales").show();    
                      
                    } else {                
                      $("#h1ErrorSales").html('Ha ocurrido un error, por favor volver a intentar.');  
                      $("#btnSalesHome").hide();      
                      $("#btnSalesReload").show();    
                      $("#errorSales").show();
                    }
                    app.mobileApp.pane.loader.hide();
                  },
                  error: function (xhr, err) {
                    app.mobileApp.pane.loader.hide();
                    $("#h1ErrorSales").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
                    $("#btnSalesHome").hide();      
                    $("#btnSalesReload").show();    
                    $("#errorSales").show();
                  }
                });
            }else{
                $("#h1ErrorSales").html(Object['msg']);  
                $("#btnSalesHome").hide();      
                $("#btnSalesReload").show();    
                $("#errorSales").show();
            }
          
        } else {                
          $("#h1ErrorSales").html('Ha ocurrido un error, por favor volver a intentar.');  
          $("#btnSalesHome").hide();      
          $("#btnSalesReload").show();    
          $("#errorSales").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorSales").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnSalesHome").hide();      
        $("#btnSalesReload").show();    
        $("#errorSales").show();
      }
    });
    
  }
});

function goSales2(clubid){
    localStorage.setItem("clubid_sales",clubid);
    app.mobileApp.navigate("components/sales_2/view.html");
}