'use strict';

app.reserve_eval = kendo.observable({
  afterShow: function () {},
  onShow: function () {
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('evaluations','Evaluaciones');
      
    var token = localStorage.getItem('token'), 
        userid = userid = window.atob(localStorage.getItem('uKey'));
      
    $.ajax({
      type: "GET",
      url: www+"canEnterToGym?userid=" + userid,
      headers: { 'authorization': 'token ' + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#div_reserve_eval").html('');
        $("#div_eval").html('');
        $("#errorReserveEval").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var obj = Object['data'];
          var data = [];
          var select = "<div id='club_reserve_eval'><label style='color:white;'>Club a Reservar</label><input id='clubid' style='width:100%;'/></div>";
          select += "<label style='color:white;'>Día a Reservar</label><input id='reserve_date' value='" + dateNow() + "' style='width:100%'/><br><br>";
          select += "<div align='center'><button onclick='searchReserveEval()' style='width: 40%;' class='km-button km-primary' id='search_class'><i class='fa fa-search fa-lg' aria-hidden='true'></i> Buscar</button>";
          select += "<button onclick='cleanFormEval()' style='width: 40%;' class='km-button' id='clean_class'><i class='fa fa-refresh fa-lg' aria-hidden='true'></i> Limpiar</button></div></div>";  
            
          for (var index in obj['clubs']) {
            data.push({ text: obj['clubs'][index], value: index });
          }

          $("#div_reserve_eval").html(select);
          $("#reserve_date").kendoDatePicker({
            value: dateNow(),
            format: "dd-MM-yyyy",
            culture: "es-CL",
            disableDates: function (date) {
              var today = new Date();
              if (kendo.toString(new Date(date), 'yyyy-MM-dd') < kendo.toString(new Date(today), 'yyyy-MM-dd')) {
                  return true;
              } else {
                  return false;
              }
            },
            footer: false,         
          });
          $("#reserve_date").attr("readonly", true);
          if(data.length > 1){
            $("#clubid").kendoDropDownList({
              dataTextField: "text",
              dataValueField: "value",
              dataSource: data
            });    
          }else{
              $("#club_reserve_eval").attr('style','display:none;');
              $("#clubid").val(data[0].value);
          }
          
          if(obj['can_refer'] == true){
            $("#li_invite").show();    
          }else{
            $("#li_invite").hide();    
          }
            
        } else {
          $("#li_invite").hide();
            
          $("#h1ErrorReserveEval").html('Usted NO cuenta con<br>acceso al gimnasio,<br>para poder reservar evaluaciones<br>con nuestros profesores.');  
          $("#btnReserveEvalHome").hide();      
          $("#btnReserveEvalReload").show();    
          $("#errorReserveEval").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorReserveEval").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnReserveEvalHome").hide();      
        $("#btnReserveEvalReload").show();    
        $("#errorReserveEval").show();
      }
    });
    app.mobileApp.scroller().reset();
  }
});

function cleanFormEval(){
    $("#reserve_date").val(kendo.toString(new Date(), 'dd-MM-yyyy'));
    $("#div_eval").html('');
    $("#clubid").data("kendoDropDownList").select(0);
}

//buscar evaluaciones para reservar  
function searchReserveEval() {
  var token = localStorage.getItem('token'), 
      clubid = $("#clubid").val(),
      date = $("#reserve_date").val();
  $("#div_eval").html('');
      
  $.ajax({
    type: "GET",
    url: www + "getListEvaluationByDate?date=" + date + "&clubid=" + clubid,
    headers: { 'authorization': "token " + token },
    dataType: "json",
    contentType: "application/json",
    async: true,
    cache: false,
    beforeSend: function (xhr) {
      $("#errorReserveEval").hide();
      app.mobileApp.pane.loader.show();
    },
    success: function (Object) {
      if (Object['status'] == 200) {
        var data = Object['data'],
          table = "<table class='table table-condensed' style='font-size:12px;'>";
        table += "<tr><th style='color:white;border-color:#292929;'>Hora</th><th style='color:white;border-color:#292929;'>Profesor</th><th style='border-color:#292929;'>&nbsp</th></tr>";
        for (var index in data) {
          var dataBlock = data[index];
          table += "<tr>";
          table += "<td style='color:white;border-color:#292929;'>" + dataBlock['hour'] + "</td>";
          table += "<td style='color:white;border-color:#292929;'>" + dataBlock['ptname'] + "</td>";
          table += "<td style='border-color:#292929;'><div align='center'><button class='km-button km-primary' style='width:80%;' id='reserve' onclick='reserve(" + dataBlock['id'] + ",\"" + dataBlock['ptid'] + "\",\"" + dataBlock['ptname'] + "\",\"" + dataBlock['hour'] + "\",\"" + dataBlock['date'] + "\")'><i class='fa fa-check fa-lg' aria-hidden='true'></i> Reservar</button></div></td>";
          table += "</tr>";
        }
        table += "</table>";
        $("#div_eval").html("<hr style='border-color:#292929;'><h1 style='text-align:center;color:white;'>Evaluaciones Disponibles</h1><br>" + table);
      } else {
        $("#h1ErrorReserveEval").html('<br>Club sin reservas disponibles para la fecha seleccionada, por favor intente seleccionando otra fecha.');  
        $("#btnReserveEvalHome").hide();      
        $("#btnReserveEvalReload").hide();    
        $("#errorReserveEval").show();
      }
      app.mobileApp.pane.loader.hide();
    },
    error: function (xhr, err) {
      navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
      app.mobileApp.pane.loader.hide();
    }
  });
}

//reservar una hora
function reserve(id, ptid, namept, time, date) {
    
  navigator.notification.confirm(
    "¿Reservar evaluación para el día "+ date +" a las " + time + " horas, con el profesor " + namept + "?",
     confirmEval,            
    'Confirmar',           
    ['Confirmar','Cancelar']
  );
    
  function confirmEval(indexB){
      if(indexB == 1){
          var token = localStorage.getItem('token'), 
              userid = window.atob(localStorage.getItem('uKey'));
          $.ajax({
            type: "POST",
            url: www+"newEvaluation",
            headers: { 'authorization': "token " + token },
            dataType: "json",
            data: "userid=" + userid + "&ptid=" + ptid + "&roomptid=" + id,
            cache: false,
            async: true,
            beforeSend: function (xhr) {
              app.mobileApp.pane.loader.show();
            },
            success: function (Object) {
              app.mobileApp.pane.loader.hide();
              if (Object['status'] == 200) {  
                navigator.vibrate(200);
                navAlert("Aviso", 'Hora reservada!');
                app.reserve_eval.onShow();
                app.mobileApp.scroller().reset();
              } else {
                if (Object['error'] == 'ERROR-OTHER-EVALUATION') {
                  navAlert('Error', "Posee una evaluación el día " + Object['data']['taked'] + ", debe esperar hasta el día " + Object['data']['nextTake'] + ", para poder reservar otra evaluación.");
                } else{
                  navAlert('Error', 'No se encontraron datos.');
                }
              }
            },
            error: function (xhr, err) {
              navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
              app.mobileApp.pane.loader.hide();
            }
          });
      }
  }
}