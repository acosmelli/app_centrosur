'use strict';

app.history_training_pt = kendo.observable({
    afterShow: function() {},
    onShow: function() {
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
        
        goMenu('ptEnergy','PT Energy');
        
        var token = localStorage.getItem('token');
        var userid = window.atob(localStorage.getItem('uKey'));
        
        $.ajax({
          type: "GET",
          url: www+"getBlocksPTUser?userid="+userid,
          headers: { 'authorization': 'token ' + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#div_HBuy").hide();
            $("#errorHBuy").hide();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var obj = Object['data'];
              var data = [];
              for (var index in obj) {
                data.push({ club: obj[index]['club'], date: obj[index]['date'], ptname: obj[index]['ptname'], time: obj[index]['time'], qrcode: obj[index]['qrcode'], blockid: obj[index]['blockid'], status: obj[index]['status'], future: obj[index]['future'] });
              }
              
              $("#HBuy").kendoListView({
                template: kendo.template($("#template_HBuy").html()),
                dataSource: data ,
              });
                
              $("#div_HBuy").show();
                
            } else {
              $("#h1ErrorHBuy").html('NO tienes bloques<br>de PT disponibles.');  
              $("#btnHBuyHome").show();      
              $("#btnHBuyReload").hide();    
              $("#errorHBuy").show();
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            $("#h1ErrorHBuy").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
            $("#btnHBuyHome").hide();      
            $("#btnHBuyReload").show();    
            $("#errorHBuy").show();
          }
        });
        app.mobileApp.scroller().reset();      
     },
});