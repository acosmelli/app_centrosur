'use strict';

//total real de imagenes del scrollview
var count_image_total = 0;

app.home = kendo.observable({
    afterShow: function() { },
    onShow: function() {                
        var token = localStorage.getItem('token'); 
        var userid = window.atob(localStorage.getItem('uKey'));
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
                
        backMainMenu();
        
        //esto no funciona en ios hasta que este en productivo
        //checkStatusNot();
                
        ////validacion de pin
        $.ajax({
          type: "GET",
          url: www+"checkPinPass?userid="+userid,
          headers: { 'authorization': "token " + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#div_home_init").hide();
          },
          success: function (Object) {
            if (Object['status'] != 200) {
               app.mobileApp.navigate("components/savePinPass/view.html");
            }else{
                $.ajax({
                  type: "GET",
                  url: www+"getPhotoHome",
                  headers: { 'authorization': "token " + token },
                  dataType: "json",
                  contentType: "application/json",
                  async: true,
                  cache: false,
                  beforeSend: function (xhr) {
                    app.mobileApp.pane.loader.show();
                    $("#homeFinal").hide();
                  },
                  success: function (Object) {
                      //alert(Object['status']);
                    if (Object['status'] == 200) {
                       $("#homeFinal").attr('style',"height:100vh;background-size:100%;background-image: linear-gradient(to bottom, rgba(0,0,0,0.5) 0%,rgba(0,0,0,0.5) 100%), url('"+Object['photo']+"');");
                    }else{
                       $("#homeFinal").attr('style',"height:100vh;background-size:100%;background-image: linear-gradient(to bottom, rgba(0,0,0,0.5) 0%,rgba(0,0,0,0.5) 100%), url('resources/pre-login.png');");
                    }
                    $("#homeFinal").show();
                    app.mobileApp.pane.loader.hide();
                  },
                  error: function (xhr, err) {
                    app.mobileApp.pane.loader.hide();
                    $("#homeFinal").show();
                  }
                }); 
            }
            $("#div_home_init").show();
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            $("#div_home_init").show();
          }
        });    
         
    }
});