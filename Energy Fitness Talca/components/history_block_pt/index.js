'use strict';

app.history_block_pt = kendo.observable({
    afterShow: function() {},
    onShow: function() {
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
        
        goMenu('ptEnergy','PT Energy');
        
        var token = localStorage.getItem('token');
        var userid = window.atob(localStorage.getItem('uKey'));
        
        $.ajax({
          type: "GET",
          url: www+"getBlocksPT?userid="+userid,
          headers: { 'authorization': 'token ' + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#div_hblock").hide();
            $("#errorHBlockPT").hide();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var obj = Object['data'];
              var data = [];
              for (var index in obj) {
                data.push({ club: obj[index]['club'], date: obj[index]['date'], n_buy: obj[index]['n_buy'], btn: obj[index]['btn'], time: obj[index]['time'], status: obj[index]['status'], blockid: obj[index]['blockid'], real_status: obj[index]['real_status'] });
              }
              
              $("#hblock_pt").kendoListView({
                template: kendo.template($("#template_hblock_pt").html()),
                dataSource: data ,
              });
                
              $("#div_hblock").show();
                
            } else {
              $("#h1ErrorHBlockPT").html('NO tienes bloques<br>de PT disponibles.');  
              $("#btnHBlockPTHome").show();      
              $("#btnHBlockPTReload").hide();    
              $("#errorHBlockPT").show();
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            $("#h1ErrorHBlockPT").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
            $("#btnHBlockPTHome").hide();      
            $("#btnHBlockPTReload").show();    
            $("#errorHBlockPT").show();
          }
        });
        app.mobileApp.scroller().reset();      
     },
});

function statusBlock(blockid){
    navigator.notification.confirm(
      '¿Cambiar estado de venta del bloque?',
       confirmBlockStatus,            
      'Confirmar',           
      ['Confirmar','Cancelar']
    );
    
    function confirmBlockStatus(indexB){
        if(indexB == 1){
            var token = localStorage.getItem('token');
            $.ajax({
              type: "POST",
              url: www+"changeStatusBlockPT",
              headers: { 'authorization': "token " + token },
              dataType: "json",
              data: "blockid=" + blockid,
              cache: false,
              async: true,
              beforeSend: function (xhr) {
                app.mobileApp.pane.loader.show();
              },
              success: function (Object) {
                if (Object['status'] == 200) {
                  navigator.vibrate(200);
                  navAlert("Aviso", 'Estado de venta del bloque a cambiado!');
                  app.history_block_pt.onShow();
                  app.mobileApp.scroller().reset();
                } else {
                  navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                }
                app.mobileApp.pane.loader.hide();
              },
              error: function (xhr, err) {
                navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                app.mobileApp.pane.loader.hide();
              }
            });       
        }
    }   
}