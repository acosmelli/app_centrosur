'use strict';

app.newmember = kendo.observable({
  afterShow: function() { },
  onShow: function () { 
      
      app.mobileApp.scroller().reset();
      
      $("#div_newmember").hide();
      $("#div_newmember").fadeIn(1500);
  }
});

function registerMember(){
    
    var new_userid = $("#new_userid").val();
    var new_name = $("#new_name").val();
    var new_lastn = $("#new_lastn").val();
    var new_email = $("#new_email").val();
    var new_cellphone = $("#new_cellphone").val();
    var new_pass_1 = $("#new_pass_1").val();
    var new_pass_2 = $("#new_pass_2").val();
    
    //que vengna todos los campos marcados en (*)
    if(new_userid != '' && new_name != '' && new_lastn != '' && new_email != '' && new_cellphone != '' && new_pass_1 != '' && new_pass_2 !=''){
    //se valida que el correo electronico este correcto
        var bool_email = false;
        if (new_email != '') {
          var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
          if (emailRegex.test(new_email)) {
            bool_email = true;
          }
        }
        
        if(bool_email){
            //minimo 4 caracteres
            if(new_pass_1.length >= 4){
                //se valida que las contraseñas sean iguales
                if(new_pass_1 == new_pass_2){
                    //ajax para mandar datos del nuevo registro
                    $.ajax({
                      type: "POST",
                      url: www + "saveUser",
                      headers: { 'authorization': "token " + token_admin },
                      data: "new_userid=" + new_userid + "&new_name=" + new_name + "&new_lastn=" + new_lastn + "&new_email=" + new_email + "&new_cellphone=" + new_cellphone + "&new_password=" + new_pass_1,
                      dataType: "json",
                      contentType: "application/json",
                      cache: false,
                      async: true,
                      beforeSend: function (xhr) {
                        app.mobileApp.pane.loader.show();
                      },
                      success: function (Object) {
                        if (Object['status'] == 200) {
                            
                          navigator.vibrate(200);
                          
                          if(Object['data']['find'] == 0){
                              navAlert("Aviso", '¡Cliente Registrado! Espere un momento mientras lo dirigimos dentro de la aplicación.');    
                          }else{
                              navAlert("Aviso", '¡Información Actualizada! Espere un momento mientras lo dirigimos dentro de la aplicación.');    
                          }
                            
                          localStorage.setItem("uKey",window.btoa(new_userid));  
                          localStorage.setItem("pKey", window.btoa(new_pass_1));
                            
                          setTimeout(function() {
                              app.mobileApp.navigate('components/login/view.html');
                          },1000);
                          
                        } else{
                            navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                            app.mobileApp.pane.loader.hide();
                        }
                      },
                      error: function (xhr, err) {
                        app.mobileApp.pane.loader.hide();
                        navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                      }
                    }); 
                }else{
                    navAlert('Aviso','Campos contraseña deben ser iguales.');
                } 
            }else{
                navAlert('Aviso','Contraseña debe tener mínimo 4 caracteres.');
            }                  
        }else{
            navAlert('Aviso', 'Debe ingresar un correo electrónico correcto.');
        }
    }else{
        navAlert('Aviso','Debe completar los campos obligatorios (*).');
    }
    
}
