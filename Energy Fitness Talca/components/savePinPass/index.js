'use strict';

var step=1;
var pin1=null;
var pin2=null;

app.savePinPass = kendo.observable({
    aferShow: function() { },
    onShow: function() {                
        var token = localStorage.getItem('token');
        var userid = window.atob(localStorage.getItem('uKey'));
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }    
        
        changeCountNot(count_not);
        
        step=1;
        $("#passwordPin").val('');
        $("#passwordPin").focus();
        
        $.ajax({
          type: "GET",
          url: www+"checkPinPass?userid="+userid,
          headers: { 'authorization': "token " + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              $("#homePin").show();
            } else {
              $("#homePin").hide();
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
          }
        });
    }    
});

function savePin(password){
    if(password.length == 4){
        if(step==1){
            pin1=password;
            $("#textPinPass").html('Confirmar PIN');
            $("#passwordPin").val('');
            step=2;
        }else{
            pin2=password;
            if(pin1==pin2){
               var token = localStorage.getItem('token'); 
               var userid = window.atob(localStorage.getItem('uKey'));
               $.ajax({
                type: "POST",
                url: www+"savePinPass",
                headers: { 'authorization': "token " + token },
                dataType: "json",
                data: "userid=" + userid + "&pinpass=" + pin1,
                cache: false,
                async: true,
                beforeSend: function (xhr) {
                  app.mobileApp.pane.loader.show();
                },
                success: function (Object) {
                  app.mobileApp.pane.loader.hide();
                  if (Object['status'] == 200) {
                    navigator.vibrate(200);
                    app.mobileApp.navigate("components/home/view.html");
                    app.mobileApp.scroller().reset();
                  } else {
                    $("#textPinPass").html('Crear nuevo PIN');  
                    $("#passwordPin").val('');
                    step=1;
                    navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                  }
                },
                error: function (xhr, err) {
                  app.mobileApp.pane.loader.hide();
                  $("#textPinPass").html('Crear nuevo PIN');  
                  $("#passwordPin").val('');
                  $("#passwordPin").focus();
                  step=1;
                  navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                }
              });          
            }else{
              $("#textPinPass").html('Crear nuevo PIN');  
              $("#passwordPin").val('');
              $("#passwordPin").focus();
              step=1;
              navAlert('ERROR EN PIN','Los Pin ingresados no coinciden. Intente nuevamente.');
            }
        }
    }
}
