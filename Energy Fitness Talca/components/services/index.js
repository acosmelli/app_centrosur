'use strict';

app.services = kendo.observable({
  onShow: function () {
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('myProfile','Perfil');
      
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey')); 
      
    $.ajax({
      type: "GET",
      url: www+"getLastContracts?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        $("#div_select").hide();
        $("#div_home").hide();
        $("#errorServices").hide();
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
        var boolPass = false;
        if (Object['status'] == 200) {
          var contract = Object['data']['MEM'],
              contractPT = Object['data']['PT'],
              freepass = Object['data']['FP'];
            
          var data = [];
          if (isset(contract)) {
            for (var index in contract) {
              data.push({ text: contract[index]['plan'], value: index, type: 'Membresía' });
            }
          } 
          if (isset(contractPT)) {
            for (var indexPT in contractPT) {
              data.push({ text: contractPT[indexPT]['plan'], value: indexPT, type: 'Entrenamiento' });
            }
          } 
          if (isset(freepass)) {
            for (var indexFP in freepass) {
              data.push({ text: freepass[indexFP]['club'], value: indexFP, type: 'Freepass' });
            }
          }
          var div = "<div id='div_MEM'>";
          if (isset(contract)) {
            for (var index in contract) {
              boolPass = true;
              var contr = contract[index];
              var finish = contr['finish'] == '00-00-0000' ? '' : contr['finish'];
              div += "<h2 style='color:white;text-align:center;padding-top:10px;'>" + contr['plan'] + "</h2><hr style='border-color:#292929'>";
              div += "<table class='table table-condensed'>";
              div += "<tr><td style='color:white;border-color:#000000;'>Club Origen</td><td style='color:white;border-color:#000000;'>" + contr['club_start'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Nº Contrato</td><td style='color:white;border-color:#292929;'>" + index + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Estado Membresía</td><td style='color:white;border-color:#292929;'><b>" + contr['status'] + "</b></td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Inicio</td><td style='color:white;border-color:#292929;'>" + contr['start'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Término</td><td style='color:white;border-color:#292929;'>" + finish + "</td></tr>";
              var clubes = contr['access'].split(',');
              var ul = '';
              for (var i in clubes) {
                ul += "<ul>";
                ul += "<li style='color:white;'>" + clubes[i] + "</li>";
                ul += "</ul>";
              }
              div += "<tr><td style='color:white;border-color:#292929;''>Clubes con Acceso</td><td style='color:white;border-color:#292929;''>" + ul + "</td></tr>";
              div += "</table>";
              break;
            }
          } else if(isset(contractPT)){
            for (var index in contractPT) {
              boolPass = true;
              var contr = contractPT[index];
              var expired = contr['expired'] == '00-00-0000' ? '' : contr['expired'];
              div += "<h2 style='color:white;text-align:center;padding-top:10px;'>" + contr['plan'] + "</h2><hr style='border-color:#292929'>";
              div += "<table class='table table-condensed'>";
              div += "<tr><td style='color:white;border-color:#000000;'>Club Origen</td><td style='color:white;border-color:#000000;'>" + contr['club'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Nº Contrato</td><td style='color:white;border-color:#292929;'>" + index + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Inicio</td><td style='color:white;border-color:#292929;'>" + contr['start'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Expiración</td><td style='color:white;border-color:#292929;'>" + expired + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Profesor</td><td style='color:white;border-color:#292929;'>" + contr['proffesor'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Sesiones Disponibles</td><td style='color:white;border-color:#292929;'>" + contr['sessions'] + "</td></tr>";
              div += "</table>";
              break;
            }
          } else if(isset(freepass)){
             for (var index in freepass) {
              boolPass = true;
              var freep = freepass[index];
              div += "<h2 style='color:white;text-align:center;padding-top:10px;'>Freepass " + freep['club'] + "</h2><hr style='border-color:#292929'>";
              div += "<table class='table table-condensed'>";
              div += "<tr><td style='color:white;border-color:#000000;'>Club Origen</td><td style='color:white;border-color:#000000;'>" + freep['club'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Inicio</td><td style='color:white;border-color:#292929;'>" + freep['start'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Térmio</td><td style='color:white;border-color:#292929;'>" + freep['end'] + "</td></tr>";
              var clubes = freep['access'].split(',');
              var ul = '';
              for (var i in clubes) {
                ul += "<ul>";
                ul += "<li style='color:white;'>" + clubes[i] + "</li>";
                ul += "</ul>";
              }
              div += "<tr><td style='color:white;border-color:#292929;'>Clubes con Acceso</td><td style='border-color:#292929;'>" + ul + "</td></tr>";
              div += "</table>";
              break;
            } 
          }
            
          div += "</div>";
            
          if(boolPass){
              $("#div_select").show();
              $("#selectContract").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: data,
                valueTemplate: '<span>#: data.type # : #: data.text #</span>',
                template: '<span class="k-state-default"><h4>#: data.type #</h4><h5>#: data.text #</h5></span>',
              });
              
              $("#div_home").html(div);
              $("#div_home").show();
          }else{
            $("#h1ErrorServices").html('NO tienes productos<br>disponibles.');
            $("#btnServiceHome").show();      
            $("#btnServiceReload").hide();    
            $("#errorServices").show();    
          }
        } else {
          $("#h1ErrorServices").html('NO tienes productos<br>disponibles.');
          $("#btnServiceHome").show();      
          $("#btnServiceReload").hide();    
          $("#errorServices").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {     
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorServices").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnServiceHome").hide();      
        $("#btnServiceReload").show();    
        $("#errorServices").show();
      }
    });
   app.mobileApp.scroller().reset();
  }
});

function changeContract() {
  var token = localStorage.getItem('token'); 
  var userid = window.atob(localStorage.getItem('uKey'));
  $.ajax({
    type: "GET",
    url: www+"getLastContracts?userid=" + userid,
    headers: { 'authorization': "token " + token },
    dataType: "json",
    contentType: "application/json",
    async: true,
    cache: false,
    beforeSend: function (xhr) {
      $("#div_MEM").hide();
      $("#errorServices").hide();
      app.mobileApp.pane.loader.show();
    },
    success: function (Object) {
      var div = '';
      if (Object['status'] == 200) {
        var contract = Object['data']['MEM'],
            contractPT = Object['data']['PT'],
            freepass = Object['data']['FP'];
          
        if (isset(contract)) {
          for (var index in contract) {
            var contr = contract[index];
            var finish = contr['finish'] == '00-00-0000' ? '' : contr['finish'];
            if (jQuery("#selectContract").val() == index) {
              div += "<h2 style='color:white;text-align:center;padding-top:10px;'>" + contr['plan'] + "</h2><hr style='border-color:#292929'>";
              div += "<table class='table table-condensed'>";
              div += "<tr><td style='color:white;border-color:#000000;'>Club Origen</td><td style='color:white;border-color:#000000;'>" + contr['club_start'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Nº Contrato</td><td style='color:white;border-color:#292929;'>" + index + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Estado Membresía</td><td style='color:white;border-color:#292929;'><b>" + contr['status'] + "</b></td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Inicio</td><td style='color:white;border-color:#292929;'>" + contr['start'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Término</td><td style='color:white;border-color:#292929;'>" + finish + "</td></tr>";
              var clubes = contr['access'].split(',');
              var ul = '';
              for (var i in clubes) {
                ul += "<ul>";
                ul += "<li style='color:white;'>" + clubes[i] + "</li>";
                ul += "</ul>";
              }
              div += "<tr><td style='color:white;border-color:#292929;''>Clubes con Acceso</td><td style='color:white;border-color:#292929;''>" + ul + "</td></tr>";
              div += "</table>";
              break;
            }
          }
        }
        if (isset(contractPT)) {
          for (var index in contractPT) {
            var contr = contractPT[index];
            var expired = contr['expired'] == '00-00-0000' ? '' : contr['expired'];
            if (jQuery("#selectContract").val() == index) {
              div += "<h2 style='color:white;text-align:center;padding-top:10px;'>" + contr['plan'] + "</h2><hr style='border-color:#292929'>";
              div += "<table class='table table-condensed'>";
              div += "<tr><td style='color:white;border-color:#000000;'>Club Origen</td><td style='color:white;border-color:#000000;'>" + contr['club'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Nº Contrato</td><td style='color:white;border-color:#292929;'>" + index + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Inicio</td><td style='color:white;border-color:#292929;'>" + contr['start'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Expiración</td><td style='color:white;border-color:#292929;'>" + expired + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Profesor</td><td style='color:white;border-color:#292929;'>" + contr['proffesor'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Sesiones Disponibles</td><td style='color:white;border-color:#292929;'>" + contr['sessions'] + "</td></tr>";
              div += "</table>";
              break;
            }
          }
        }
        if (isset(freepass)) {
          for (var index in freepass) {
            var freep = freepass[index];
            if (jQuery("#selectContract").val() == index) {
              div += "<h2 style='color:white;text-align:center;padding-top:10px;'>Freepass " + freep['club'] + "</h2><hr style='border-color:#292929'>";
              div += "<table class='table table-condensed'>";
              div += "<tr><td style='color:white;border-color:#000000;'>Club Origen</td><td style='color:white;border-color:#000000;'>" + freep['club'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Inicio</td><td style='color:white;border-color:#292929;'>" + freep['start'] + "</td></tr>";
              div += "<tr><td style='color:white;border-color:#292929;'>Fecha Térmio</td><td style='color:white;border-color:#292929;'>" + freep['end'] + "</td></tr>";
              var clubes = freep['access'].split(',');
              var ul = '';
              for (var i in clubes) {
                ul += "<ul>";
                ul += "<li style='color:white;'>" + clubes[i] + "</li>";
                ul += "</ul>";
              }
              div += "<tr><td style='color:white;border-color:#292929;'>Clubes con Acceso</td><td style='border-color:#292929;'>" + ul + "</td></tr>";
              div += "</table>";
              break;
            }
          }
        }
        
        if(div == ''){
          $("#h1ErrorServices").html('Ha ocurrido un error, por favor volver a intentar.');
          $("#btnServiceHome").hide();      
          $("#btnServiceReload").show();    
          $("#errorServices").show();   
        }
      } else {
          $("#h1ErrorServices").html('NO tienes productos<br>disponibles.');
          $("#btnServiceHome").hide();      
          $("#btnServiceReload").show();    
          $("#errorServices").show();    
      }
      $("#div_MEM").html(div);
      $("#div_MEM").show();
      app.mobileApp.pane.loader.hide();
    },
    error: function (xhr, err) {
      app.mobileApp.pane.loader.hide();
      $("#h1ErrorServices").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
      $("#btnServiceHome").hide();      
      $("#btnServiceReload").show();    
      $("#errorServices").show();
    }
  });
}