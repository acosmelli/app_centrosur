'use strict';

app.service_survey = kendo.observable({
    afterShow: function() { },
    onShow: function() {                
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
        
        backMainMenu();
        
        var token = localStorage.getItem('token');
        var userid = window.atob(localStorage.getItem('uKey'));
               
        //clean vars survey
        $("#clubid_ss").val('-');
        $("#question1").val(0);
        $("#question2").val(0);
        $("#question3").val(0);
        $("#question4").val(0);
        $("#question5").val(0);
        $("#question6").val(0);
        $("#question7").val(0);
        $("#question8").val(0);
        $("#question9").val(0);
        $("#question10").val(0);
        $("#question11").val('-');
        $("#question12").val('-');
        $("#question13").val('-');
        
        cleanStar();
        
        $.ajax({
          type: "GET",
          url: www+"contentSurvey?userid="+userid,
          headers: { 'authorization': 'token ' + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#li_survey").hide();
              
            jQuery("#surveyDIV").find("div[id*='step']").each(function(indexdiv,objDiv){
               $(objDiv).hide(); 
            });
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var obj = Object['data'];
              var data = [];
              data.push({ text: 'Seleccione..', value: '-' });
              for (var index in obj['clubs']) {
                data.push({ text: obj['clubs'][index], value: index });
              }

              $("#clubid_ss").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                valueTemplate: '<span style="color:white;">#: data.text #</span>',
                dataSource: data,
              });
              
              var dataQ = [];
              dataQ.push({ text: 'Seleccione..', value: '-' });
              dataQ.push({ text: 'SI', value: 1 });
              dataQ.push({ text: 'NO', value: 0 });
                
              $("#question11").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                valueTemplate: '<span style="color:white;">#: data.text #</span>',
                dataSource: dataQ,
              });
                
              $("#question12").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                valueTemplate: '<span style="color:white;">#: data.text #</span>',
                dataSource: dataQ,
              });
                
              $("#question13").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                valueTemplate: '<span style="color:white;">#: data.text #</span>',
                dataSource: dataQ,
              });
              $("#li_survey").show();
                              
              $("#step1").fadeIn(1500);
            }else{
                navAlert('Aviso','Usted NO puede realizar la encuentas porque no cumple con los requisitos minimos.');
                app.mobileApp.navigate('components/home/view.html');
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
          }
        });
    }
});

function step1(){
    $("#step2").hide();
    $("#step1").fadeIn(1500);
    app.mobileApp.scroller().reset();      
}

function step2(){
   var clubid = $("#clubid_ss").val(); 
    if(clubid!='-'){
        $("#step1").hide();
        $("#step2").fadeIn(1500);
        $("#step3").hide();
        app.mobileApp.scroller().reset();      
    }else{
        navAlert('Error','Debes seleccionar el club para comenzar la encuesta.');
    }
}

function step3(){
    $("#step2").hide();
    $("#step3").fadeIn(1500);
    $("#step4").hide();
}

function step4(){
    $("#step3").hide();
    $("#step4").fadeIn(1500);
    $("#step5").hide();
}

function step5(){
    $("#step4").hide();
    $("#step5").fadeIn(1500);
    $("#step6").hide();
}

function step6(){
    $("#step5").hide();
    $("#step6").fadeIn(1500);
    $("#step7").hide();
}

function step7(){
    $("#step6").hide();
    $("#step7").fadeIn(1500);
    $("#step8").hide();
}

function step8(){
    $("#step7").hide();
    $("#step8").fadeIn(1500);
    $("#step9").hide();
}

function step9(){
    $("#step8").hide();
    $("#step9").fadeIn(1500);
    $("#step10").hide();
}

function step10(){
    $("#step9").hide();
    $("#step10").fadeIn(1500);
    $("#step11").hide();
}

function step11(){
    $("#step10").hide();
    $("#step11").fadeIn(1500);
    $("#step12").hide();
}

function step12(){
    $("#step11").hide();
    $("#step12").fadeIn(1500);
}

function finishSurvey(){
   //validar que las preguntas esten respondidas
   var question11 = $("#question11").val();
   var question12 = $("#question12").val();
   var question13 = $("#question13").val();
   if(question11 != '-' && question12 != '-' && question13 != '-'){
       //armar datos para enviar la encuesta a la api
       var clubid = $("#clubid_ss").val();
       
       var question1 = $("#question1").val();
       var question2 = $("#question2").val();
       var question3 = $("#question3").val();
       var question4 = $("#question4").val();
       var question5 = $("#question5").val();
       var question6 = $("#question6").val();
       var question7 = $("#question7").val();
       var question8 = $("#question8").val();
       var question9 = $("#question9").val();
       var question10 = $("#question10").val();
              
       //ajax to send survey
       var token = localStorage.getItem('token'); 
       var userid = window.atob(localStorage.getItem('uKey'));
       
       $.ajax({
          type: "POST",
          url: www + "sendSurvey",
          headers: { 'authorization': "token " + token },
          data: "userid=" + userid + "&clubid=" + clubid + "&question1=" + question1 + "&question2=" + question2 + "&question3=" + question3 + "&question4=" + question4 + "&question5=" + question5+ "&question6=" + question6 + "&question7=" + question7+ "&question8=" + question8 + "&question9=" + question9 + "&question10=" + question10 + "&question11=" + question11 + "&question12=" + question12 + "&question13=" + question13,
          dataType: "json",
          contentType: "application/json",
          cache: false,
          async: true,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
               $('#formSurvey').trigger("reset");
              navigator.vibrate(200);
              navAlert("Aviso", 'Encuesta enviada!');
              setTimeout(function(){
                app.mobileApp.scroller().reset();
                app.mobileApp.navigate("components/home/view.html");
              },1500);
            } else {
              navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            navAlert('Error: ' + xhr.status + ' / ' + err, 'Problemas de conectividad, intente mas tarde...');
          }
        });    
       
    }else{
        navAlert('Error','Para continuar debe responder las preguntas de alternativas.');
    }
}

function setStar(star,id_hidden){
    var tempValue = $("#"+id_hidden).val();
    if(star == tempValue){
        jQuery("#surveyDIV").find("i[id*='"+id_hidden+"_']").each(function(index,obj){
            var idtemp = $(obj).attr('id').split('_');
            if(idtemp[1] <= star){
                $("#"+id_hidden+"_"+idtemp[1]).attr('style','color:white;');
            }
        });
        $("#"+id_hidden).val(0);
    }else{
        jQuery("#surveyDIV").find("i[id*='"+id_hidden+"_']").each(function(index,obj){
            var idtemp = $(obj).attr('id').split('_');
            if(idtemp[1] <= star){
                $("#"+id_hidden+"_"+idtemp[1]).attr('style','color:yellow;');
            }else{
                $("#"+id_hidden+"_"+idtemp[1]).attr('style','color:white;');
            }
        });
        $("#"+id_hidden).val(star);
    }   
}

function cleanStar(){
    jQuery("#surveyDIV").find("i[id*='question']").each(function(index,obj){
        $(obj).attr('style','color:white;');
    });
}