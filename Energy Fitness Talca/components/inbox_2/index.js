'use strict';

app.inbox_2 = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('myProfile','Perfil');
      
    var notid = localStorage.getItem('notid');
    var token = localStorage.getItem('token');
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www+"getNotificationGeneric?notid=" + notid + "&userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#div_inbox_2").hide();
        $("#errorInbox2").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
            
          checkNotPending();
            
          var notification = Object['data'];
          var data = [];  
            
          var message = Base64.decode(notification['message']);
          var title = window.JSON.parse(notification['title']);
            
          data.push({ name: name_user, title: title, date: notification['date'], time: notification['time'] });
                        
          $("#div_inbox_2").kendoListView({
            template: kendo.template($("#template_inbox_2").html()),
            dataSource: data ,
          });
            
          if(notification['type'] == 'P'){
              $("#btnInbox2").attr('onclick',"goHref('components/payments/view.html')");
              $("#spanInbox2").html('PAGAR');
              $("#btnInbox2").show();
          }else{
              $("#spanInbox2").html('&nbsp;');
              $("#btnInbox2").removeAttr('onclick');
              $("#btnInbox2").hide();
          }
            
          $("#msg_push_1").html(message);
          $("#div_inbox_2").show();     
        } else {
          $("#h1ErrorInbox2").html('Ha ocurrido un error al tratar<br>de cargar la notificación.');  
          $("#btnInbox2Home").hide();      
          $("#btnInbox2Reload").show();    
          $("#errorInbox2").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorInbox2").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnInbox2Home").hide();      
        $("#btnInbox2Reload").show();    
        $("#errorInbox2").show();
      }
    });
  app.mobileApp.scroller().reset();      
  }
});