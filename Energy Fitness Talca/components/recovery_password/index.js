'use strict';

app.recoverypassword = kendo.observable({
  afterShow: function() { },
  onShow: function () {
      
      app.mobileApp.scroller().reset();
      
      $("#div_recovery_p1").hide();
      $("#div_recovery_p2").hide();
      
      $("#div_recovery_p1").fadeIn(1500);
      
      /*clean vars form1*/
        $("#userid_request_rp1").val('');
        $("#email_request_rp1").val('');
      /*clean vars form1*/
      
      /*clean vars form2*/
        $("#userid_request_rp2").val('');
        $("#type_doc_rp2").val('');
        $("#number_serie_rp2").val('');
        $("#email_request_rp2").val('');
      /*clean vars form2*/
  }
});

function recoveryPassword1(){
    var userid_request = $("#userid_request_rp1").val();
    var email_request = $("#email_request_rp1").val();
    if(userid_request != '' && email_request != ''){
        //se valida correo electronico que sea valido
        var bool_email = false;
        if (email_request != '') {
          var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
          if (emailRegex.test(email_request)) {
            bool_email = true;
          }
        }
        
        if(bool_email){
            $.ajax({
              type: "POST",
              url: www + "recoveryPassword1",
              headers: { 'authorization': "token " + token_admin },
              data: "userid_request=" + userid_request + "&email_request=" + email_request,
              dataType: "json",
              contentType: "application/json",
              cache: false,
              async: true,
              beforeSend: function (xhr) {
                  app.mobileApp.pane.loader.show();
              },
              success: function (Object) {
                if (Object['status'] == 200) {
                  navigator.vibrate(200);
                  navAlert("Aviso", '¡Contraseña recuperada! Hemos envíado a su correo electrónico una nueva contraseña.');
                  app.mobileApp.navigate('components/login/view.html');
                } else if (Object['status'] == 201) {
                  //aca se esconde el primer div de solicitar y se muestra el segundo que pide codigo de serie
                    
                  var dataTypeDoc = [];
                  dataTypeDoc.push({ text: "Cédula Chilena", value: "CEDULA" });
                  dataTypeDoc.push({ text: "Cédula a Extranjero", value: "CEDULA_EXT" });
                    
                  $("#type_doc_rp2").kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "value",
                    valueTemplate: '<span style="color:white;">#: data.text #</span>',
                    dataSource: dataTypeDoc,
                  });
                    
                  $("#div_recovery_p1").fadeOut(600);
                  setTimeout(function() {
                    $("#userid_request_rp2").val(userid_request);
                    $("#div_recovery_p2").fadeIn(1000);
                  },800);
                    
                }else{
                   if (Object['error'] == 'ERROR-SEND-MAIL') {    
                       navAlert('Error','Ha ocurrido un error al enviar su nueva contraseña al correo electrónico, por favor vuelva a intentarlo más tarde.');
                   }else if (Object['error'] == 'ERROR-NOT-USER') {    
                       navAlert('Error','¡Lo sentimos! El RUN ingresado no registra en nuestro sistema, por favor diríjase a su club para validar esta información.');
                   }else{
                       navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                   }
                }
                app.mobileApp.pane.loader.hide();
              },
              error: function (xhr, err) {
                app.mobileApp.pane.loader.hide();
                navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
              }
            });   
        }else{
            navAlert('Aviso', 'Debe ingresar un correo electrónico correcto.');
        }
    }else{
        navAlert('Aviso', 'Debe completar formulario.');
    }
}

function recoveryPassword2(){
    var userid_request = $("#userid_request_rp2").val();
    var type_request = $("#type_doc_rp2").val();
    var serial_request = $("#number_serie_rp2").val();
    var email_request = $("#email_request_rp2").val();
    
    if(userid_request != '' && serial_request != '' && email_request != ''){
        //se valida correo electronico que sea valido
        var bool_email = false;
        if (email_request != '') {
          var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
          if (emailRegex.test(email_request)) {
            bool_email = true;
          }
        }
        
        if(bool_email){
            $.ajax({
              type: "POST",
              url: www + "recoveryPassword2",
              headers: { 'authorization': "token " + token_admin },
              data: "userid_request=" + userid_request + "&email_request=" + email_request + "&type_request=" + type_request + "&serial_request=" + serial_request,
              dataType: "json",
              contentType: "application/json",
              cache: false,
              async: true,
              beforeSend: function (xhr) {
                app.mobileApp.pane.loader.show();
              },
              success: function (Object) {
                if (Object['status'] == 200) {
                  navigator.vibrate(200);
                  navAlert("Aviso", '¡Contraseña recuperada! Hemos envíado a su correo electrónico una nueva contraseña.', "success");
                  app.mobileApp.navigate('components/login/view.html');
                } else{
                   if (Object['error'] == 'ERROR-SEND-MAIL') {    
                       navAlert('Error','Ha ocurrido un error al enviar su nueva contraseña al correo electrónico, por favor vuelva a intentarlo más tarde.');
                   }else if (Object['error'] == 'ERROR-SERIE-NUMBER') {    
                       navAlert('Error','El número de serie ingresado no coincide. Por favor vuelva a intentarlo o diríjase a su club para actualizar su correo electrónico.');
                   }else{
                       navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
                   }
                }
                app.mobileApp.pane.loader.hide();
              },
              error: function (xhr, err) {
                app.mobileApp.pane.loader.hide();
                navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
              }
            }); 
        }else{
            navAlert('Aviso', 'Debe ingresar un correo electrónico correcto.');
        }
    }else{
        navAlert('Aviso', 'Debe completar formulario.');
    }
}
