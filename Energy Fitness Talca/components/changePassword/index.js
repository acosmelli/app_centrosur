'use strict';

app.changepassword = kendo.observable({
    afterShow: function() {},
    onShow: function() { 
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
        
        goMenu('myProfile','Perfil');
        
        var force_change = localStorage.getItem('force_change');
        if(force_change == 'Y'){
            $("#msg_force_change").show();
        }else{
            $("#msg_force_change").hide();
        }
        
        /*clean vars*/
          $("#actual_password").val('');
          $("#new_password_1").val('');
          $("#new_password_2").val('');
        /*clean vars*/
    }
});

function changePassword(){
    var actual_password = $("#actual_password").val();
    var new_password_1 = $("#new_password_1").val();
    var new_password_2 = $("#new_password_2").val();
    
    if(actual_password != '' && new_password_1 != '' && new_password_2 != ''){
        if(new_password_1.length >= 4){
             if(new_password_1 == new_password_2){
                var token = localStorage.getItem('token'); 
                var userid = window.atob(localStorage.getItem('uKey'));
                
                navigator.notification.confirm(
                  "¿Actualizar contraseña?",
                   onChangePass,            
                  'Confirmar',           
                  ['Confirmar','Cancelar']
                );
                
                function onChangePass(indexB){
                    if(indexB == 1){
                       $.ajax({
                          type: "POST",
                          url: www + "changePassword",
                          headers: { 'authorization': "token " + token },
                          data: "userid=" + userid + "&actual_password=" + actual_password + "&new_password=" + new_password_1,
                          dataType: "json",
                          contentType: "application/json",
                          cache: false,
                          async: true,
                          beforeSend: function (xhr) {
                            app.mobileApp.pane.loader.show();
                          },
                          success: function (Object) {
                            if (Object['status'] == 200) {
                              navigator.vibrate(200);
                              navAlert("Aviso", 'Contraseña actualizada!');
                              localStorage.setItem("pKey", window.btoa(new_password_1));
                              localStorage.setItem("force_change", 'N');
                              app.mobileApp.navigate('components/home/view.html');    
                            } else {
                                if (Object['error'] == 'ERROR-SAME'){
                                    navAlert('Error', 'Contraseña nueva debe ser distinta a la actual.');      
                                }else if (Object['error'] == 'ERROR-NOT-PASS'){
                                    navAlert('Error', 'Contraseña actual incorrecta.');      
                                }else{
                                    navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');      
                                }
                            }

                            app.mobileApp.pane.loader.hide();
                          },
                          error: function (xhr, err) {
                            app.mobileApp.pane.loader.hide();
                            navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                          }
                        }); 
                    }  
                }
             }else{
                 navAlert('Aviso','Campos nueva contraseña deben coincidir.');
             }
        }else{
            navAlert('Aviso','La nueva contraseña debe tener un mínimo de 4 caracteres.');
        }
    }else{
        navAlert('Aviso','Completar Formulario.');
    }
}
