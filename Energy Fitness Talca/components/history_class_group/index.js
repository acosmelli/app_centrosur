'use strict';

app.history_class_group = kendo.observable({
  afterShow: function () {},
  onShow: function () {
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('classGroup','Clases Grupales');
      
    var token = localStorage.getItem('token'), 
        userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www + "getMyLastReservesClassGroup?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        $("#div_list_class").hide();
        $("#errorHistoryReserveClass").hide();
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var obj = Object['data'],
              div = '';
          for (var id in obj) {
            var enrollid = obj[id];
            div += "<div class='panel panel-default'><div class='panel-body'><table class='table table-condensed'>";
            div += "<tr><td>Club</td><td>" + enrollid['club'] + "</td></tr>";
            div += "<tr><td>Sala</td><td>" + enrollid['room_name'] + "</td></tr>";
            div += "<tr><td>Clase</td><td>" + enrollid['name'] + "</td></tr>";
            div += "<tr><td>Profesor</td><td>" + enrollid['proff'] + "</td></tr>";
            div += "<tr><td>Fecha</td><td>" + enrollid['date'] + ' ' + enrollid['hour'] + "</td></tr>";
            if(enrollid['seatid'] > 0){
                div += "<tr><td>Nº Asiento</td><td>" + enrollid['seatid'] + "</td></tr>";
            }
            div += "</table></div></div>";
          }
          $("#div_list_class").html("<br><em style='color:white;'>(*) Ultimos registros de clases grupales.</em><br><br>" + div);
          $("#div_list_class").show();
        } else {
          $("#h1ErrorHistoryReserveClass").html('NO tienes reservas<br>pasadas de clases grupales.');  
          $("#btnHistoryReserveClassHome").show();      
          $("#btnHistoryReserveClassReload").hide();    
          $("#errorHistoryReserveClass").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorHistoryReserveClass").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnHistoryReserveClassHome").hide();      
        $("#btnHistoryReserveClassReload").show();    
        $("#errorHistoryReserveClass").show();
      }
    });
   app.mobileApp.scroller().reset();
  }
});