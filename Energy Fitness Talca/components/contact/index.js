'use strict';

app.contact = kendo.observable({
    afterShow: function() {},
    onShow: function() {
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
        
        backMainMenu();
        
        var token = localStorage.getItem('token');
        
        $.ajax({
          type: "GET",
          url: www+"getAllClubs",
          headers: { 'authorization': 'token ' + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#div_contact").hide();
            $("#errorContact").hide();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var obj = Object['data'];
              var data = [];
              for (var index in obj['clubs']) {
                data.push({ text: obj['clubs'][index], value: index });
              }
                
              $("#clubid_contact").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                valueTemplate: '<span style="color:white;">#: data.text #</span>',
                dataSource: data,
              });
                
              $("#div_contact").show();
                
            } else {                
              $("#h1ErrorContact").html('Ha ocurrido un error, por favor volver a intentar.');  
              $("#btnContactHome").show();      
              $("#btnContactReload").hide();    
              $("#errorContact").show();
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            $("#h1ErrorContact").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
            $("#btnContactHome").hide();      
            $("#btnContactReload").show();    
            $("#errorContact").show();
          }
        });
     },
});

function sendMsg(){
    var clubid = $("#clubid_contact").val();
    var subject = $("#subject_contact").val();
    var message = $("#message_contact").val();
    
    if(clubid != '' && subject != '' && message != ''){
        var token = localStorage.getItem('token'), 
            userid = userid = window.atob(localStorage.getItem('uKey'));    
        
        $.ajax({
          type: "POST",
          url: www+"sendContact",
          headers: { 'authorization': "token " + token },
          dataType: "json",
          data: "userid=" + userid + "&subject=" + subject + "&clubid=" + clubid + "&message=" + message,
          cache: false,
          async: true,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
          },
          success: function (Object) {
            app.mobileApp.pane.loader.hide();
            if (Object['status'] == 200) {
              navigator.vibrate(200);
              navAlert("Aviso", 'Mensaje enviado!');
              /*clean vars*/
                $("#clubid_contact").val('')
                $("#subject_contact").val('')
                $("#message_contact").val('')
              /*clean vars*/
              app.contact.onShow();
              app.mobileApp.scroller().reset();
            } else {
              navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
            app.mobileApp.pane.loader.hide();
          }
        });    
        
    }else{
        navAlert('Aviso', 'Debe completar formulario.');
    }
}