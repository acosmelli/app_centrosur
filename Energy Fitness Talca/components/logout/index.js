'use strict';

app.logout = kendo.observable({
    onShow: function() {
      localStorage.clear();  
      sessionStorage.clear();  
      $("#div_signin").hide();
      setTimeout(function(){app.mobileApp.navigate('components/login/view.html')},0);
    },
    afterShow: function() {}
});
