'use strict';

app.training_pt = kendo.observable({
    afterShow: function() {},
    onShow: function() {
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
        
        goMenu('ptEnergy','PT Energy');
        
        $("#content_modal_pt").html('');
        $("#clubid_training").val('');
        
        var blockidTemp = localStorage.getItem('blockid');
        var timeTemp = localStorage.getItem('time');
        if(blockidTemp!=null && timeTemp!=null){
            localStorage.removeItem("blockid");
            localStorage.removeItem("time");
            app.mobileApp.pane.loader.show();
            buyBlock(blockidTemp,timeTemp);
        }
        
        var token = localStorage.getItem('token');
        
        $.ajax({
          type: "GET",
          url: www+"getAllClubs",
          headers: { 'authorization': 'token ' + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#div_training").hide();
            $("#div_pts").hide();
            $("#errorTrainingPT").hide();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var obj = Object['data'];
              var data = [];
              for (var index in obj['clubs']) {
                data.push({ text: obj['clubs'][index], value: index });
              }
                
              $("#clubid_training").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: data,
                optionLabel: 'Seleccione..',
              });
                
              $("#div_training").show();
                
            } else {
              $("#h1ErrorTrainingPT").html('Ha ocurrido un error, por favor volver a intentar.');  
              $("#btnTrainingPTHome").hide();      
              $("#btnTrainingPTReload").show();    
              $("#errorTrainingPT").show();
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            $("#h1ErrorTrainingPT").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
            $("#btnTrainingPTHome").hide();      
            $("#btnTrainingPTReload").show();    
            $("#errorTrainingPT").show();
          }
        });
        app.mobileApp.scroller().reset();      
     },
});

function loadPT(clubid){
    if(clubid != ''){
        var token = localStorage.getItem('token'); 
        var userid = window.atob(localStorage.getItem('uKey'));
        $.ajax({
          type: "GET",
          url: www+"getPtsAvailable?userid=" + userid + "&clubid=" + clubid,
          headers: { 'authorization': "token " + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#div_pts").hide();
            $("#errorTrainingPT").hide();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var book_pts = Object['data'];
              var data = [];  
              for(var pt_count in book_pts){
                  var photo = 'resources/no-photo.jpg';
                  if(book_pts[pt_count]['photo'] != null){
                     photo = book_pts[pt_count]['photo'];   
                  }
                  
                  var modal = '<!-- modal select ' + book_pts[pt_count]['blockid'] + ' -->';
                  modal+= '<div id="modal_pt_'+book_pts[pt_count]['blockid']+'" style="display:none;text-align:center;">';
                  modal+= '<ul data-role="listview" data-style="inset" style="font-size:10px">';
                  for(var index in book_pts[pt_count]['hours']){
                      var block = book_pts[pt_count]['hours'][index];
                      modal+= '<li style="list-style:none;">';
                      modal+= '<input onclick="takePT(\''+book_pts[pt_count]['name']+'\','+book_pts[pt_count]['blockid']+',\''+book_pts[pt_count]['money_text']+'\')" type="radio" name="block_hour_' + book_pts[pt_count]['blockid'] + '" id="' + book_pts[pt_count]['blockid'] + '_' + index + '" value="' + block['start'] + '" class="km-radio">';
                      modal+= '<label class="km-radio-label" for="' + book_pts[pt_count]['blockid'] + '_' + index + '">' + block['start'] + ' / ' + block['end'] + '</label>';
                      modal+= '</li>';
                  }
                  modal+= '</ul>';
                  //modal+= '<br><button onclick="cancelModalBlock('+book_pts[pt_count]['blockid']+')" class="km-button km-primary" style="width:50%;background-color:#ccc;border-color:#ccc;">Cerrar</button>';
                  modal+= '</div>';
                  
                  $("#content_modal_pt").append( modal );
                                    
                  data.push({ 
                      photo: photo, 
                      blockid: book_pts[pt_count]['blockid'], 
                      name: book_pts[pt_count]['name'], 
                      time: book_pts[pt_count]['time'],
                      date: book_pts[pt_count]['date'], 
                      date_name: book_pts[pt_count]['date_name'],
                      money: book_pts[pt_count]['money_text']
                  });
              }
                            
              $("#ul_pts").kendoListView({
                template: kendo.template($("#template_ul_pts").html()),
                dataSource: data ,
              });
                
              $("#div_pts").show();
                         
            } else {
              $("#h1ErrorTrainingPT").html('Club seleccionado sin<br>profesores disponibles,<br>vuelve a intentar mas tarde...');
              $("#btnTrainingPTHome").show();      
              $("#btnTrainingPTReload").hide();    
              $("#errorTrainingPT").show();
              //$("#clubid_training").data("kendoDropDownList").value("");
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            $("#h1ErrorTrainingPT").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
            $("#btnTrainingPTHome").show();      
            $("#btnTrainingPTReload").hide();    
            $("#errorTrainingPT").show();
          }
        });
    }else{
        $("#div_pts").hide();
        $("#errorTrainingPT").hide();
    }
}

function selectTime(blockid){
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www+"getMyCreditCards?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {},
      success: function (Object) {
        if (Object['status'] == 200) {
                                    
          $("#modal_pt_"+blockid).kendoWindow({
            title: "Horario Disponible",
            modal: true,
            draggable: false,
            width: "90%"  
          }).data("kendoWindow").center().open();
                     
        } else {
          navigator.notification.confirm(
           "Para poder comprar una sesión con nuestros profesores, debes tener al menos una tarjeta de crédito registrada!",
             goWebPay,            
            'Importante',           
            ['Agregar']
          );
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');        
      }
    });
}

function goWebPay(){
    app.mobileApp.navigate('components/webpay/view.html');
}

function cancelModalBlock(blockid){
  $("#modal_pt_"+blockid).data("kendoWindow").close();    
  $("input[name='block_hour_" + blockid + "']").attr("checked", false);
}

function takePT(name,blockid,money_text){
    if($("input[name='block_hour_" + blockid + "']:radio").is(':checked')){
      $("#modal_pt_"+blockid).data("kendoWindow").close();
      var time = $("input[name='block_hour_" + blockid + "']:checked").val();
      var date_name = $("#date_name_"+blockid).val();
      navigator.notification.confirm(
        "¿Entrenar con el profesor " + name + " el día " + date_name + " a las " + time + " horas? Esta sesión tiene un valor de $ " + money_text + " pesos.",
         onTake,            
        'Comprar',           
        ['Comprar','Cancelar']
      );
    
      function onTake(indexB){
          if(indexB == 1){
              localStorage.setItem("blockid",blockid);
              localStorage.setItem("time",time);
              checkPinPass('training_pt');
          }
      } 
    }else{
       navAlert('Aviso','Debe seleccionar un horario!');       
    }
}

function buyBlock(blockid,time){
    var token = localStorage.getItem('token'), 
        userid = window.atob(localStorage.getItem('uKey'));
    var date = $("#date_"+blockid).val();
    $.ajax({
      type: "POST",
      url: www+"takePT",
      headers: { 'authorization': "token " + token },
      dataType: "json",
      data: "userid=" + userid + "&blockid=" + blockid + "&time=" + time + "&date=" + date,
      cache: false,
      async: true,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        //$("#modal_pt_"+blockid).data("kendoWindow").close();    
        //$("input[name='block_hour_" + blockid + "']").attr("checked", false);
      },
      success: function (Object) {
        app.mobileApp.pane.loader.hide();
        if (Object['status'] == 200) {
          navigator.vibrate(200);
          navAlert("Aviso", '¡Has comprado la sesión PT seleccionada con el profesor '+name+'!'); 
          app.mobileApp.scroller().reset();
          setTimeout(
            function(){
                app.mobileApp.navigate('components/history_training_pt/view.html');
            },1500);
        } else {
            if(Object['error'] == 'ERROR-TAKED'){
                navAlert('Error', 'El bloque que usted intenta comprar ya se encuentra ocupado por otra persona.. favor intentar con otro bloque.');
            }else if(Object['error'] == 'ERROR-MONEY-DAY-MAX'){//máximo monto diario de pago excedido
                navAlert('Error', 'Usted ha excedido el monto máximo diario a utilizar en su tarjeta de crédito.');
            }else if(Object['error'] == 'ERROR-MONEY-EXCEEDED'){//máximo monto de pago excedido
                navAlert('Error', 'Usted ha excedido el monto máximo a utilizar en su tarjeta de crédito.');
            }else if(Object['error'] == 'ERROR-MONEY-COUNT'){//máxima cantidad de pagos diarios excedido
                navAlert('Error', 'Usted ha excedido los pagos diarios a realizar en su tarjeta de crédito.');
            }else{
                navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
            }
        }
      },
      error: function (xhr, err) {
        navAlert('Error: ' + xhr.status + ' / ' + err, 'Problemas de conectividad, intente mas tarde...');
        app.mobileApp.pane.loader.hide();
      }
    });       
}