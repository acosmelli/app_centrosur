'use strict';

app.webpay = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
      
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('myProfile','Perfil');
            
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "GET",
      url: www+"getMyCreditCards?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#div_cards").hide();
        $("#errorWebpay").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var ccards = Object['data'];
          var data = [];  
            
          for(var index in ccards){
              var src = 'resources/card_oneclick/gen.png';
              if(ccards[index]['card'] == 'VISA'){
                  src = 'resources/card_oneclick/visa.png';
              }else if(ccards[index]['card'] == 'AMERICANEXPRESS'){
                  src = 'resources/card_oneclick/american.png';
              }else if(ccards[index]['card'] == 'MASTERCARD'){
                  src = 'resources/card_oneclick/mastercard.png';
              }else if(ccards[index]['card'] == 'DINERS'){
                  src = 'resources/card_oneclick/diners.png';
              }else if(ccards[index]['card'] == 'MAGNA'){
                  src = 'resources/card_oneclick/magna.png';
              }
              data.push({ photo: src, last4: ccards[index]['last4'], cardid: ccards[index]['id'], principal: ccards[index]['principal'] });
          }
                       
          $("#ul_cards").kendoListView({
            template: kendo.template($("#template_ul_cards").html()),
            dataSource: data ,
          });
          $("#div_cards").show();
                     
        } else {
          $("#h1ErrorWebpay").html('NO tienes tarjetas<br>de crédito asociadas.');  
          $("#btnWebpayHome").hide();      
          $("#btnWebpayReload").show();    
          $("#errorWebpay").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorWebpay").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnWebpayHome").hide();      
        $("#btnWebpayReload").show();    
        $("#errorWebpay").show();
      }
    });
  app.mobileApp.scroller().reset();      
  }
});

function deleteCard(cardid){
    navigator.notification.confirm(
      "¿Eliminar tarjeta de crédito seleccionada?",
       confirmDeleteCard,            
      'Confirmar',           
      ['Cancelar','Confirmar']
    );
    
    function confirmDeleteCard(indexB){
        if(indexB == 2){
            localStorage.setItem('card_id',cardid);
            checkPinPass('deleteCard');
        }
    }
}

function deleteCard2(cardid){
    var token = localStorage.getItem('token');
    var userid = window.atob(localStorage.getItem('uKey'));

    $.ajax({
      type: "POST",
      url: www+"removeCard",
      headers: { 'authorization': "token " + token },
      dataType: "json",
      data: "cardid=" + cardid + '&userid=' + userid,
      cache: false,
      async: true,
      beforeSend: function (xhr) {
          app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
          if (Object['status'] == 200) {
            navigator.vibrate(200); 
            app.webpay.onShow();
          }else{
            navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
          }
          app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
          navAlert('Error: ' + xhr.status + ' / ' + err, 'Problemas de conectividad, intente mas tarde...');
          app.mobileApp.pane.loader.hide();
      }
    }); 
}

function setCard(cardid){
    navigator.notification.confirm(
      "¿Marcar tarjeta de crédito como principal?",
       confirmSetCard,            
      'Confirmar',           
      ['Cancelar','Confirmar']
    );
    
    function confirmSetCard(indexB){
        if(indexB == 2){
           localStorage.setItem('card_id',cardid);
           checkPinPass('setCard');
        }
    }
}

function setCard2(cardid){
    var token = localStorage.getItem('token');
    var userid = window.atob(localStorage.getItem('uKey'));
    $.ajax({
      type: "POST",
      url: www+"setCard",
      headers: { 'authorization': "token " + token },
      dataType: "json",
      data: "cardid=" + cardid + '&userid=' + userid,
      cache: false,
      async: true,
      beforeSend: function (xhr) {
          app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
          app.mobileApp.pane.loader.hide();
          if (Object['status'] == 200) {
              navigator.vibrate(200);    
              app.webpay.onShow();
          }else{
              navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
          }
          
      },
      error: function (xhr, err) {
          navAlert('Error: ' + xhr.status + ' / ' + err, 'Problemas de conectividad, intente mas tarde...');
          app.mobileApp.pane.loader.hide();
      }
    });  
}

function addCreditCard(){
    var token = localStorage.getItem('token'); 
    var userid = window.atob(localStorage.getItem('uKey'));
  
    //se chequea que tenga el pin pass si no debe registrarlo y asi se abrira la ventana para agregar
    $.ajax({
      type: "GET",
      url: www+"checkPinPass?userid="+userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
           //pide el pin para mandar al initoneclick
           checkPinPass('webpayInit');
        } else {
            app.mobileApp.navigate("components/savePinPass/view.html");
            localStorage.setItem('goPageWebpayNew','webpayInit');
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
      }
    }); 
    //si lo tiene que lo ingrese y asi dice si tiene el perfil listo o no 
}

function initOneClick(){
  var token = localStorage.getItem('token'); 
  //var userid = window.atob(localStorage.getItem('uKey'));
      
  $.ajax({
    type: "GET",
    url: www +'initOneClick',
    headers: { 'authorization': "token "+ token },
    dataType: "json",
    contentType: "application/json",
    async: true,
    beforeSend: function(xhr) {
      $("#webpay").hide();
      app.mobileApp.pane.loader.show();
    },
    success: function(Object) {
      if (Object['status'] == 200) {
        var count=0;
        var win = window.open( "components/webpay/location.html?action="+ Object['data']['urlwebpay']  +"&token="+ Object['data']['token'] +"&username=", "_blank",
                "location=yes,EnableViewPortScale=yes" );

        // Once the InAppBrowser finishes loading
        win.addEventListener("loadstop",function(){
          // Clear out the name in localStorage for subsequent opens.
          win.executeScript({code:"localStorage.setItem('status','');" });

          // Start an interval
          var loop = setInterval(function(){
            // Execute JavaScript to check for the existence of a name in the
            // child browser's localStorage.
            win.executeScript(
              {code: "localStorage.getItem('status')"},
              function(values){
                var status = values[0];
                // If a name was set, clear the interval and close the InAppBrowser.
                if( status && status !='' ){
                  win.close();
                  clearInterval(loop);

                  if(status=='OK' && count==0){
                    //alert  
                    navAlert('Aviso','Tarjeta registrada con éxito!');
                    count=1;
                    app.webpay.onShow();
                  }else if(count==0){
                    //alert  
                    navAlert('Error','La tarjeta no pudo ser registrada.');
                    count=1;  
                  }
                  
                }
              }
            );
          });
        });

        app.mobileApp.navigate('components/webpay/view.html');
        app.webpay.onShow();
      } else {
        navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
      }
      app.mobileApp.pane.loader.hide();
    },
    error: function (xhr, err) {
      app.mobileApp.pane.loader.hide();
      navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
    }
  });
  app.mobileApp.scroller().reset();
}