'use strict';

app.history_routine_3 = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    goMenu('myRoutine','Mi Rutina');
      
    var token = localStorage.getItem('token'); 
    var routineid = localStorage.getItem('routineid'); 
    var count_machine = localStorage.getItem('count_machine');   
    var machine_name = localStorage.getItem('name_machine'); 
    $("#title_div_hr3").html('Máquina y/o Ejercicio '+count_machine);
      
    $.ajax({
      type: "GET",
      url: www+"getMachine?routineid=" + routineid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
        $("#div_image_rt3").hide();
        $("#div_info_rt3").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {

          var machine = Object['data'];
          var div_img = "";
          for (var i=1;i<=4;i++) {
            if(machine['photo'+i] != ''){
              div_img += "<div data-role='page' style='text-align:center;'>";
              div_img += "<br><label>" + machine_name + "</label><br>";    
              div_img += "<img style='width:150px;height:180px;' src='" + machine['photo'+i] + "'>";
              div_img += "</div>";
            }
          }
            
          if(div_img == ''){
            div_img += "<div data-role='page' style='text-align:center;'>";
            div_img += "<br><label>" + machine_name + "</label><br>";    
            div_img += "<img style='width:150px;height:180px;' src='../resources/no-image.png'>";
            div_img += "</div>";      
          }
                    
          $("#div_image_rt3").html(div_img);
          $("#div_image_rt3").kendoMobileScrollView();
          $("#div_image_rt3").show();
          $("#div_image_rt3").data("kendoMobileScrollView").viewShow();
            
          var table = "<table class='table table-condensed'>";
          table += "<tr><td style='color:white;border-color:#292929;'>Series / Tiempo [Min]</td><td style='color:white;border-color:#292929;'>" + machine['serie'] + "</td></tr>";
          table += "<tr><td style='color:white;border-color:#292929;'>Repeticiones</td><td style='color:white;border-color:#292929;'>" + machine['repeat'] + "</td></tr>";
          table += "<tr><td style='color:white;border-color:#292929;'>Peso [KG] / Nivel</td><td style='color:white;border-color:#292929;'>" + machine['weight'] + "</td></tr>";
          table += "<tr><td style='color:white;border-color:#292929;'>Comentario</td><td style='color:white;border-color:#292929;'>" + machine['comment'] + "</td></tr>";
          table += "</table>";
          
          $("#div_info_rt3").html(table);
          $("#div_info_rt3").show();
          
        } else {
          navAlert('Aviso', 'Ha ocurrido un error al tratar de cargar la máquina y/o ejercicio.. favor volver a intentar.');
          app.mobileApp.navigate("components/history_routine_2/view.html");
          app.history_routine_2.onShow();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');        
        app.mobileApp.navigate("components/history_routine_2/view.html");
        app.history_routine_2.onShow();
      }
    });
    app.mobileApp.scroller().reset();      
  }
});