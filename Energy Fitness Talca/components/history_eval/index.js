'use strict';

app.history_eval = kendo.observable({
  onShow: function () {
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
    
    goMenu('evaluations','Evaluaciones');
      
    var token = localStorage.getItem('token'), 
        userid = window.atob(localStorage.getItem('uKey'));

    $.ajax({
      type: "GET",
      url: www+"getMyEvaluations?userid=" + userid,
      headers: { 'authorization': "token " + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        $("#chartEval").hide();
        $("#div_myEvaluation").hide();
        $("#errorHistoryEval").hide();
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
          var obj = Object['data'],
              div = '';

          var per_grease = new Array();
          var per_muscle = new Array();
          var per_water = new Array();
          var per_weight = new Array();
          var arr_date = new Array();
          var per_imc = new Array();

          for (var id in obj) {
            var evaluation = obj[id];
            div += "<div class='panel panel-default'><div class='panel-body'><table class='table table-condensed'>";
            div += "<tr><td>Club</td><td>" + evaluation['club_name'] + "</td></tr>";
            div += "<tr><td>Fecha</td><td>" + evaluation['date'] + "</td></tr>";
            div += "<tr><td>Peso</td><td>" + evaluation['weight'] + "</td></tr>";
            div += "<tr><td>IMC</td><td>" + evaluation['imc'] + "</td></tr>";
            div += "<tr><td>% Grasa actual</td><td>" + evaluation['per_grease'] + "%</td></tr>";
            div += "<tr><td>% Muscular actual</td><td>" + evaluation['per_muscle'] + "%</td></tr>";
            div += "<tr><td>% Agua actual</td><td>" + evaluation['per_water'] + "%</td></tr>";
            div += "<tr><td colspan='2'><div align='center'><button style='width:80%;' href='#' onClick='window.open(\"" + evaluation['link'] + "\", \"_system\")' class='km-button km-primary'><i class='fa fa-cloud-download fa-lg' aria-hidden='true'></i> Descargar Evaluación</button></div></td></tr>";
            div += "</table></div></div>";

            //arreglos para el grafico
            per_grease.push(evaluation['per_grease']);
            per_muscle.push(evaluation['per_muscle']);
            per_water.push(evaluation['per_water']);
            per_weight.push(evaluation['weight']);
            arr_date.push(evaluation['date']);
            per_imc.push(evaluation['imc']);
          }
          
          $("#chartEval").kendoChart({
            title: {
              text: "Ultimas 12 Evaluaciones"
            },
            legend: {
              position: "bottom"
            },
            chartArea: {
              background: ""
            },
            seriesDefaults: {
              type: "line",
              style: "smooth"
            },
            series: [{
              name: "Peso",
              data: per_weight.reverse()
            }, {
              name: "IMC",
              data: per_imc.reverse()
            }, {
              name: "% Grasa Actual",
              data: per_grease.reverse()
            }, {
              name: "% Muscular Actual",
              data: per_muscle.reverse()
            }],
            valueAxis: {
              labels: {
                format: "{0}"
              },
              line: {
                visible: false
              },
              axisCrossingValue: -0
            },
            categoryAxis: {
              categories: arr_date.reverse(),
              majorGridLines: {
                visible: false
              },
              labels: {
                rotation: "auto"
              }
            },
            tooltip: {
              visible: true,
              format: "{0}",
              template: "#= series.name #: #= value #"
            }
          });
            
          $("#chartEval").show();            
          $("#chartEval").data("kendoChart").redraw(); // para que se adapte de nuevo a la pagina
          $("#div_myEvaluation").html("<hr style='border-color:#292929;' />" + div);
          $("#div_myEvaluation").show();
        } else {
          $("#h1ErrorHistoryEval").html('NO tienes evaluaciones<br>disponibles.');  
          $("#btnHistoryEvalHome").show();      
          $("#btnHistoryEvalReload").hide();    
          $("#errorHistoryEval").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorHistoryEval").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnHistoryEvalHome").hide();      
        $("#btnHistoryEvalReload").show();    
        $("#errorHistoryEval").show();
      }
    });
   app.mobileApp.scroller().reset();
  },
  afterShow: function () { }
});