'use strict';

app.docs_schedule = kendo.observable({
    afterShow: function() {},
    onShow: function() { 
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
        
        goMenu('classGroup','Clases Grupales');
        
        var token = localStorage.getItem('token');
        
        $.ajax({
          type: "GET",
          url: www+"getAllClubs",
          headers: { 'authorization': 'token ' + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#div_docs_schedule").hide();
            $("#errorDocsSchedule").hide();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var obj = Object['data'];
              var data = [];
              var pass = false;
              for (var index in obj['schedule']) {
                data.push({ link: obj['schedule'][index], clubname: obj['clubs'][index] });
                pass = true;
              }              
              
              if(pass){
                $("#div_docs_schedule").kendoListView({
                  template: kendo.template($("#template_docs_schedule").html()),
                  dataSource: data ,
                });
                  
                $("#div_docs_schedule").show();    
              }else{
                $("#h1ErrorDocsSchedule").html('Horarios de clases grupales aún no se encuentran disponibles, por favor intentar mas tarde...');  
                $("#btnDocsScheduleHome").show();      
                $("#btnDocsScheduleReload").hide();    
                $("#errorDocsSchedule").show();
              }  
            } else {                
              $("#h1ErrorDocsSchedule").html('Ha ocurrido un error, por favor volver a intentar.');  
              $("#btnDocsScheduleHome").hide();      
              $("#btnDocsScheduleReload").show();    
              $("#errorDocsSchedule").show();
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            $("#h1ErrorDocsSchedule").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
            $("#btnDocsScheduleHome").hide();      
            $("#btnDocsScheduleReload").show();    
            $("#errorDocsSchedule").show();
          }
        });
    }
});

function openSchedule(link){
    window.open(link, '_system');
}