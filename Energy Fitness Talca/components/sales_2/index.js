'use strict';

app.sales_2 = kendo.observable({
  afterShow: function () {},
  onShow: function (){ 
    //name and photo user
    var name_user = localStorage.getItem('name_user');
    $("#name_menu").html('&nbsp;'+name_user);            
    var photo = localStorage.getItem('photo_user');
    if (photo && substr(photo, 0, 4) == 'data'){
       $("#photo_menu").attr('src',photo);       
    }
      
    changeCountNot(count_not);
      
    cleanBuyPack();            
    blockBtnCart(0);
           
    var clubid = localStorage.getItem('clubid_sales');
    var type = localStorage.getItem('type_sales');
    var date_start = localStorage.getItem('date_start_sales');
      
    $("#clubid_plan").val(clubid);      
    $("#date_start_plan").val(date_start);      
    $("#type_plan").val(type);      
      
    var date_start_text = localStorage.getItem('date_start_text_sales');
    $("#date_start_span").html(date_start_text);
    var token = localStorage.getItem('token');
    var userid = window.atob(localStorage.getItem('uKey'));
      
    $.ajax({
      type: "GET",
      url: www+"getProductSales?userid="+userid+"&clubid="+clubid+"&type="+type,
      headers: { 'authorization': 'token ' + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
          $("#errorSales_2").hide();
          $("#div_sales_2").hide();
      },
      success: function (Object) {
        if (Object['status'] == 200) {
            var obj = Object['data'];
            var data = [];
            var count = 0;
            
            for (var index in obj) {
                var ch = obj[index];
                data.push({ id: ch['id'], name: ch['name'], money: ch['money'], money_text: ch['money_text'], time: ch['time'], count: count });
                count++;
            }             
                        
            $("#div_prod_sales").kendoListView({
              template: kendo.template($("#template_sales_2").html()),
              dataSource: data ,
            });
              
            $("#div_sales_2").show();    
          
        } else {                
          $("#h1ErrorSales_2").html('Ha ocurrido un error,<br>por favor volver a intentar.');  
          $("#btnSales_2Home").hide();      
          $("#btnSales_2Reload").show();    
          $("#errorSales_2").show();
        }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        app.mobileApp.pane.loader.hide();
        $("#h1ErrorSales_2").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
        $("#btnSales_2Home").hide();      
        $("#btnSales_2Reload").show();    
        $("#errorSales_2").show();
      }
    });
  }
});

function selectPlan(planid,name,money,money_text,time){
    var tempPlanid = $('#planid').val();
    if(tempPlanid==planid){
        //se limpian variables
        $('#planid').val(0);
        $('#name_plan').val(0);
        $('#money_plan').val(0);
        $('#money_text_plan').val(0);
        $('#time_plan').val(0);
        
        //lista en nada
        $("#plan_"+tempPlanid).attr('style','background-color:#000000;list-style:none;padding:10px 0px 10px 10px;border-bottom: 1px solid grey;');
        
        //boton
        $("#btnBuyPlan").attr('style','width: 100%;border-radius:0px;background-color:#292929;border-color:#292929;');
        $("#btnBuyPlan").removeAttr('onclick');
    }else{
        $('#div_prod_sales').find("li[id*='plan_']").each(function(index,li){        
            if( $(li).attr('id') == 'plan_'+planid ){
                $(li).attr('style','background-color:#990000;list-style:none;padding:10px 0px 10px 10px;border-bottom: 1px solid grey;');
            }else{
                $(li).attr('style','background-color:#000000;list-style:none;padding:10px 0px 10px 10px;border-bottom: 1px solid grey;');
            }
        });
        
        $('#planid').val(planid);
        $('#name_plan').val(name);
        $('#money_plan').val(money);
        $('#money_text_plan').val(money_text);
        $('#time_plan').val(time);
        
        //se habilita boton
        $("#btnBuyPlan").attr('style','width: 100%;border-radius:0px;background-color:#990000;border-color:#990000;');
        $("#btnBuyPlan").attr('onclick','buyPlan()');
    }
}

function cleanBuyPack(){
    $('#div_prod_sales').find("li[id*='plan_']").each(function(index,li){        
        $(li).attr('style','background-color:#000000;list-style:none;padding:10px 0px 10px 10px;border-bottom: 1px solid grey;');
    });

    $('#planid').val(0);
    $('#name_plan').val(0);
    $('#money_plan').val(0);
    $('#money_text_plan').val(0);
    $('#time_plan').val(0);
    
    $("#btnBuyPlan").attr('style','width: 100%;border-radius:0px;background-color:#292929;border-color:#292929;');
    $("#btnBuyPlan").removeAttr('onclick');
}

function blockBtnCart(action){
    //bloquear
    if(action==1){
        $("#btnBuyPlan").attr('disabled','disabled');
    }else{//habilitar
        $("#btnBuyPlan").removeAttr('disabled');
    }    
}

function buyPlan(){
    blockBtnCart(1);
    
    var planid = $('#planid').val();
    var name_plan = $('#name_plan').val();
    var money_plan = $('#money_plan').val();
    var money_text_plan = $('#money_text_plan').val();
    var time_plan = $('#time_plan').val();
    if(planid!=0 && name_plan!=0 && money_plan!=0 && money_text_plan!=0 && time_plan!=0){
        var token = localStorage.getItem('token'); 
        var userid = window.atob(localStorage.getItem('uKey'));
        $.ajax({
          type: "GET",
          url: www+"getMyCreditCards?userid=" + userid,
          headers: { 'authorization': "token " + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {},
          success: function (Object) {
              if (Object['status'] == 200) {     
                  //aca ver lo del descuento!
                  localStorage.setItem("planid",planid);
                  localStorage.setItem("money_plan",money_plan);
                  navigator.notification.confirm(
                    "Comprar plan "+name_plan+" ("+time_plan+"), tiene un costo de $"+money_text_plan+". ¿Desea continuar?",
                     onTake,            
                    'Comprar',           
                    ['Cancelar','Comprar']
                  );                       
              } else {
                  blockBtnCart(0);
                  navigator.notification.confirm(
                   "Para poder comprar un plan, debes tener al menos una tarjeta de crédito registrada!",
                     goWebPay,            
                    'Importante',           
                    ['Agregar']
                  );
              }
              app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            //navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');        
          }
        });           
    }else{
        navAlert('Aviso','Debes seleccionar un plan para continuar.');
    } 
}

function onTake(indexB){
    if(indexB == 2){
      checkPinPass('sales_2');
    }else{
      blockBtnCart(0);
    }
}

function finalBuy(planid,money){
    var token = localStorage.getItem('token');
    var userid = window.atob(localStorage.getItem('uKey'));
    
    var type = $("#type_plan").val();
    var clubid = $("#clubid_plan").val();
    var date_start = $("#date_start_plan").val();
    
    $.ajax({
      type: "POST",
      url: www+"buyPlan",
      headers: { 'authorization': "token " + token },
      dataType: "json",
      data: "userid=" + userid + "&planid=" + planid + '&money='+money+"&clubid="+clubid+"&start="+date_start+"&type="+type,
      cache: false,
      async: true,
      beforeSend: function (xhr) {
        app.mobileApp.pane.loader.show();
      },
      success: function (Object) {
          if (Object['status'] == 200) {
              navigator.vibrate(200);
              navAlert("Aviso", '¡Plan ha sido comprado exitosamente!'); 
              app.sales_2.onShow();
              app.mobileApp.scroller().reset();
          } else {
              if(Object['error'] == 'ERROR-STOCK'){
                  navAlert('Error', 'Plan ya no se encuentra disponible.');
              }else if(Object['error'] == 'ERROR-MONEY-DAY-MAX'){//máximo monto diario de pago excedido
                  navAlert('Error', 'Usted ha excedido el monto máximo diario a utilizar en su tarjeta de crédito.');
              }else if(Object['error'] == 'ERROR-MONEY-EXCEEDED'){//máximo monto de pago excedido
                  navAlert('Error', 'Usted ha excedido el monto máximo a utilizar en su tarjeta de crédito.');
              }else if(Object['error'] == 'ERROR-MONEY-COUNT'){//máxima cantidad de pagos diarios excedido
                  navAlert('Error', 'Usted ha excedido los pagos diarios a realizar en su tarjeta de crédito.');
              }else{
                  navAlert('Error', 'Ha ocurrido un error, por favor volver a intentar.');
              }
          }
        app.mobileApp.pane.loader.hide();
      },
      error: function (xhr, err) {
        navAlert('Error: ' + xhr.status + ' / ' + err, 'Problemas de conectividad, intente mas tarde...');
        app.mobileApp.pane.loader.hide();
      }
    });  
}