'use strict';

app.burn_pt = kendo.observable({
    afterShow: function() {},
    onShow: function() {
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
          
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }   
        
        changeCountNot(count_not);
                
        var token = localStorage.getItem('token');
        var userid = window.atob(localStorage.getItem('uKey'));
        
        $.ajax({
          type: "GET",
          url: www+"getPTsUser?userid="+userid,
          headers: { 'authorization': 'token ' + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
            $("#div_burn_pt").hide();
            $("#errorBurnPT").hide();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var obj = Object['data'];
              var data = [];
              for (var index in obj) {
                data.push({ 
                    club: obj[index]['club'], 
                    date: obj[index]['date'], 
                    ptname: obj[index]['ptname'], 
                    time: obj[index]['time'], 
                    qrcode: obj[index]['qrcode'], 
                    type: obj[index]['type'], 
                    future: obj[index]['future'],
                    ss_disp: obj[index]['ss_disp'],
                    expired: obj[index]['expired'],
                    uniqid: obj[index]['uniqid'],
                    ptid: index
                });
                                  
                if(obj[index]['qrcode'] != null){
                  var modal = '<!-- modal select ' + index + ' -->';
                  modal+= '<div id="modal_burn_pt_'+index+'" style="display:none;text-align:center;">';
                  modal+= '<h2>Muestra el siguiente código QR a tu entrenador para hacer efectiva la Sesión PT.</h2>';
                  modal+= '<img src="'+obj[index]['qrcode']+'" style="max-width:100%;" />';
                  modal+= '</div>';
                  
                  $("#content_modal_burn_pt").append( modal );
                }
              }
              
              $("#qr_burn_pt").kendoListView({
                template: kendo.template($("#template_burn_pt").html()),
                dataSource: data ,
              });
                
              $("#div_burn_pt").show();
                
            } else {
              $("#h1ErrorBurnPT").html('NO tienes sesiones<br>disponibles a quemar.');  
              $("#btnBurnPTHome").show();      
              $("#btnBurnPTReload").hide();    
              $("#errorBurnPT").show();
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
            $("#h1ErrorBurnPT").html('Error: ' + xhr.status + ' / ' + err +'<br>Imposible conectarse al servidor, intente mas tarde...');  
            $("#btnBurnPTHome").hide();      
            $("#btnBurnPTReload").show();    
            $("#errorBurnPT").show();
          }
        });
        app.mobileApp.scroller().reset();      
     },
});

var timeoutQR = null;
function showQRPT(ptid,type,uniqid){
   $("#modal_burn_pt_"+ptid).kendoWindow({
      title: "Activar Sesión",
      modal: true,
      draggable: false,
      width: "90%"  ,
      close: function() {
        clearTimeout(timeoutQR);
        app.burn_pt.onShow();
        app.mobileApp.scroller().reset();  
      },
    }).data("kendoWindow").center().open();
      
    timeoutQR = setTimeout(function(){checkPTQR(ptid,type,uniqid)},3000);
   /*if (window.navigator.simulator === true) {
       navAlert('Aviso','Simulador NO Soporta BarcodeScanner');
   }else{ 
       cordova.plugins.diagnostic.isLocationAvailable(function(available){
         if(available){
            navigator.geolocation.getCurrentPosition(function(position){
                var userid = window.atob(localStorage.getItem('uKey'));
                var token = localStorage.getItem('token');
                var latitude = 0;//position.coords.latitude;
                var longitude = 0;//position.coords.longitude;
                    
                $.ajax({
                  type: "GET",
                  url: www+"checkPositionUser?userid="+userid+"&blockid="+blockid+"&latitude="+latitude+"&longitude="+longitude,
                  headers: { 'authorization': "token " + token },
                  dataType: "json",
                  contentType: "application/json",
                  async: true,
                  cache: false,
                  beforeSend: function (xhr){},
                  success: function (Object){
                      if(Object['status'] == 200){
                          $("#modal_hblock_pt_"+blockid).kendoWindow({
                            title: "Activar Sesión",
                            modal: true,
                            draggable: false,
                            width: "90%"  
                          }).data("kendoWindow").center().open();
                            
                          setTimeout(function(){checkBlockQr(blockid)},3000);
                      }else if(Object['error'] == 'ERROR-GPS'){
                          navAlert('Error','Para poder hacer efectiva la quema de sesión debes estar en nuestras dependencias.');      
                      }
                  },
                  error: function (xhr, err){
                    //navAlert('Error: ' + xhr.status + ' / ' + err, 'Imposible conectarse al servidor, intente mas tarde...');
                  }
                });
            },function(error){
                //error al tratar de tomar la geolocalizacion actual
                navAlert('Error [2]: ' + error.code, error.message);
            },{maximumAge: 30000, timeout: 3000, enableHighAccuracy: true});    
         }else{
             navAlert('Aviso',"Para poder utilizar el código QR debes tener activado el GPS.");
         }
      }, function(error){
          //error al ver si el gps estaba activado
          navAlert('Error [1]: ' + error.code, error.message);
      });
   }*/
}

function checkPTQR(ptid,type,uniqid){
    var token = localStorage.getItem('token');
    $.ajax({
      type: "GET",
      url: www+"checkPTQR?id="+ptid + "&type=" + type + "&uniqid=" + uniqid,
      headers: { 'authorization': 'token ' + token },
      dataType: "json",
      contentType: "application/json",
      async: true,
      cache: false,
      beforeSend: function (xhr) {},
      success: function (Object) {
        if (Object['status'] == 200) {
            
          $("#modal_burn_pt_"+ptid).data("kendoWindow").close();    
          setTimeout(function(){
              navigator.vibrate(200);
              navAlert('Aviso','Código QR ha sido aceptado! Ya puedes utilizar nuestras dependencias, cualquier duda consultar a tu entrenador...');
              app.burn_pt.onShow();
              app.mobileApp.scroller().reset();  
                
              clearTimeout(timeoutQR);
          },1000);
            
        } else {
          timeoutQR = setTimeout(function(){checkPTQR(ptid,type,uniqid)},3000);
        }
      },
      error: function (xhr, err) {
          timeoutQR = setTimeout(function(){checkPTQR(ptid,type,uniqid)},3000);
      }
    });
}