'use strict';

var step=1;
var pin1=null;
var pin2=null;

app.checkPinPass = kendo.observable({
    aferShow: function() { },
    onShow: function() {                
        /*var token = localStorage.getItem('token');
        var userid = window.atob(localStorage.getItem('uKey'));*/
        var name_user = localStorage.getItem('name_user');
        var photo = localStorage.getItem('photo_user');
        $("#name_menu").html('&nbsp;'+name_user);            
        if (photo && substr(photo, 0, 4) == 'data'){
           $("#photo_menu").attr('src',photo);       
        }    
        
        changeCountNot(count_not);
        
        $("#passwordPin2").val('');
        $("#passwordPin2").focus();
        
        var token = localStorage.getItem('token');
        var userid = window.atob(localStorage.getItem('uKey'));  
        
        $.ajax({
          type: "GET",
          url: www+"checkPinPass?userid="+userid,
          headers: { 'authorization': "token " + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
          },
          success: function (Object) {
              if (Object['status'] != 200) {
                  localStorage.setItem('actionBtnSavePinPass',1);
                  app.mobileApp.navigate('components/savePinPass/view.html');
              }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
          }
        });
        var page = localStorage.getItem('pagePinPass');
        if(page =='webpayInit'){
            $("#cancelBtnPin").attr('onclick','goWebPay();');
        }else if(page =='setCard'){
            $("#cancelBtnPin").attr('onclick','goWebPay();');
        }else if(page =='deleteCard'){
            $("#cancelBtnPin").attr('onclick','goWebPay();');
        }else if(page =='sales_2'){
            $("#cancelBtnPin").attr('onclick',"app.mobileApp.navigate('components/sales_2/view.html');");
        }else if(page =='payments'){
            $("#cancelBtnPin").attr('onclick',"app.mobileApp.navigate('components/payments/view.html');");
        }else{
            $("#cancelBtnPin").attr('onclick',"app.mobileApp.navigate('components/home/view.html');");
        }
    }    
});

function checkPinView(password){
    if(password.length == 4){
        var token = localStorage.getItem('token');
        var userid = window.atob(localStorage.getItem('uKey'));
        $.ajax({
          type: "GET",
          url: www+"checkPinPass?userid="+userid+'&pinpass='+password,
          headers: { 'authorization': "token " + token },
          dataType: "json",
          contentType: "application/json",
          async: true,
          cache: false,
          beforeSend: function (xhr) {
            app.mobileApp.pane.loader.show();
          },
          success: function (Object) {
            if (Object['status'] == 200) {
              var pagePinPass = localStorage.getItem('pagePinPass');
              checkPinPass2(pagePinPass);
            } else {
              $("#passwordPin2").val('');
              $("#passwordPin2").focus();
              navAlert('Error','PIN ingresado no es el correcto. Intente nuevamente.');
            }
            app.mobileApp.pane.loader.hide();
          },
          error: function (xhr, err) {
            app.mobileApp.pane.loader.hide();
          }
        });
    }
}